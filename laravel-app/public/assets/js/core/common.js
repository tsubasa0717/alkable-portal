
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

(function($){

    //

})(jQuery);

function searchWord(search_range){

    var searchText = $('.fm_search').val(),
        hitIndex = [];

    $('.list_no_tr').css('display','none');

    if (searchText != '')
    {
        for (var i = 0; i < search_range.length; i++)
        {
            $('.' + search_range[i]).each(function (idx)
            {
                targetText = $(this).text();
                if (targetText.indexOf(searchText) != -1)
                {
                    hitIndex.push(idx);
                }
            });
        }

        $('.list_tr').css('display','none');

        if(hitIndex.length == 0)
        {
            $('.list_no_tr').css('display','table-row');
        }
        else
        {
            for (var i = 0; i < hitIndex.length; i++){
                $('.list_tr').eq(hitIndex[i]).css('display','table-row');
            }
        }
    }
    else {
        $('.list_tr').css('display','table-row');
    }
}