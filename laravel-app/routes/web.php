<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// 認証関連
Route::get('/login','Auth\LoginController@showLoginForm');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout','Auth\LoginController@logout')->name('logout');
Auth::routes();

Route::group(['middleware' => ['auth']], function () {

    // ダッシュボード
    Route::get('/', 'Dashboard\DashboardController@index');

    // プロジェクト管理
    Route::get('/project', 'Project\ProjectController@index');
    Route::get('/project/create', 'Project\ProjectController@create');
    Route::post('/project/store', 'ReservationManagementController@storeProject');
    Route::get('/project/edit', 'Project\ProjectController@edit');
    Route::post('/project/update', 'Project\ProjectManagementController@update');
    Route::post('/project/checkHoliday', 'Project\ProjectManagementController@checkHoliday');
    Route::get('/project/show', 'Project\ProjectController@show');

    // 顧客管理
    Route::get('/client', 'Client\ClientController@index');
    Route::get('/client/create', 'Client\ClientController@create');
    Route::post('/client/store', 'ReservationManagementController@storeClient');
    Route::post('/client/storeQuick', 'ReservationManagementController@storeQuickClient');
    Route::get('/client/edit', 'Client\ClientController@edit');
    Route::post('/client/update', 'Client\ClientManagementController@update');
    Route::post('/client/show', 'Client\ClientManagementController@getClientData');

    // 請求管理
    Route::get('/invoice', 'Invoice\InvoiceController@index');
    Route::get('/invoice/create', 'Invoice\InvoiceController@create');
    Route::post('/invoice/store', 'ReservationManagementController@storeInvoice');
    Route::get('/invoice/edit', 'Invoice\InvoiceController@edit');
    Route::post('/invoice/update', 'Invoice\InvoiceManagementController@update');
    Route::post('/invoice/show', 'Invoice\InvoiceManagementController@getInvoiceData');

    // 入金管理
    Route::post('/payment', 'Payment\PaymentManagementController@payment');

    // 社員管理
    Route::get('/account','Account\AccountController@index');
    Route::get('/account/create','Account\AccountController@create');
    Route::post('/account/store','Account\AccountManagementController@store');
    Route::get('/account/edit','Account\AccountController@edit');
    Route::post('/account/update','Account\AccountManagementController@update');
    Route::get('/account/show', function () {

        $page = [
            'title' => '社員詳細',
            'dir' => ['account', 'show'],
        ];

        $data = [
            'page' => $page,
        ];

        return view('account/show')->with($data);
    });


    // 利用日当日
    Route::get('/use', 'Production\ProductionController@index');
    Route::post('/use/startUse', 'Production\ProductionManagementController@startUse');
    Route::post('/use/checkExtension', 'Production\ProductionManagementController@checkExtension');
    Route::post('/use/doExtension', 'Production\ProductionManagementController@doExtension');
    Route::post('/use/endStandbyUse', 'Production\ProductionManagementController@endStandbyUse');
    Route::post('/use/endUse', 'Production\ProductionManagementController@endUse');

});

// デバック
Route::get('/tr','Debug\DebugController@index');

// 初期画面
Route::get('/home', 'HomeController@index')->name('home');
