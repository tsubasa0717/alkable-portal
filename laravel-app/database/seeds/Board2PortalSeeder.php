<?php

use Illuminate\Database\Seeder;
use \App\Http\Controllers\Board\BoardController;

class Board2PortalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Board顧客 → ポータルDB顧客適応
        BoardController::optimizationClients();

        // Boardプロジェクト → ポータルDB顧客適応
        BoardController::optimizationInvoices();

        // Boardユーザ → ポータルDB社員適応
        BoardController::optimizationAccounts();

        // Boardプロジェクト → ポータルDBプロジェクト適応
        BoardController::optimizationProjects();

    }
}