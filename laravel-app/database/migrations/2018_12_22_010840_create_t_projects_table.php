<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_projects', function (Blueprint $table) {
            $table->increments('project_id');
            $table->string('project_uuid');
            $table->string('project_name');
            $table->string('google_calendar_code');
            $table->integer('client_id');
            $table->integer('account_id');
            $table->integer('reservation_status');
            $table->string('project_comment')->nullable();
            $table->timestamp('start_time')->useCurrent();
            $table->timestamp('termination_time')->useCurrent();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_projects');
    }
}
