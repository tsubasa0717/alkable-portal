<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_invoices', function (Blueprint $table) {
            $table->increments('invoice_id');
            $table->string('invoice_uuid');
            $table->integer('board_project_code');
            $table->integer('project_id');
            $table->integer('board_invoice_code');
            $table->integer('board_estimate_code');
            $table->integer('board_receipt_code');
            $table->string('invoice_name');
            $table->integer('invoice_type');
            $table->integer('invoice_amount');
            $table->integer('invoice_remain_amount');
            $table->integer('invoice_status');
            $table->string('invoice_comment')->nullable();;
            $table->timestamp('invoice_deadline')->useCurrent();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_invoices');
    }
}
