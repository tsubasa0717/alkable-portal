<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_clients', function (Blueprint $table) {
            $table->increments('client_id');
            $table->integer('board_client_code');
            $table->string('client_uuid');
            $table->string('client_name');
            $table->string('client_name_disp')->nullable();
            $table->string('client_email')->nullable();
            $table->string('client_tel')->nullable();
            $table->string('client_comment')->nullable();
            $table->integer('is_black');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_clients');
    }
}
