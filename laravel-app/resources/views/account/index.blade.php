@extends('template.master')
@section('contents')
<!-- Content area -->
<div class="row">
    <div class="col-lg-7">
        <div class="panel panel-success">
            <div class="panel-heading">
                <div style="position:absolute;">{{ $page['title'] }}</div>
                <div style="text-align: right"><span class="responsive-display-inline">検索： </span><input class="fm_search" type="text" placeholder="検索ワード" style="height:20px"></div>
            </div>
            <table id="datatables" class="table table-striped table-bordered table-hover table-account">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>社員名</th>
                    <th>TEL</th>
                    <th>メールアドレス</th>
                    <th></th>
                </tr>
                </thead>
                <tbody id="sort_table">
                @foreach($accounts as $key => $account)
                <tr class="list_tr">
                    <td>{{ $key+1 }}</td>
                    <td class="tx-account_name tx_link">{{ $account->last_name }} {{ $account->first_name }}</td>
                    <td class="tx-account_tel">{{ $account->tel }}</td>
                    <td class="tx-account_email">{{ $account->email }}</td>
                    <td>@if($account->account_id == $myAccount->account_id)<button type="button" class="btn-edit btn btn-primary">編集</button>@endif</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="responsive-display heading-text pull-right" style="margin-top:-10px;margin-bottom:50px;padding-right:10px;"><a href="/account/create"><img src="/assets/img/btn/plus.png" width="50px" height="50px"></a></div>
    </div>
</div>
<!-- /main charts -->

<!-- Modal windoow -->
<div id="modal" class="modal modal-client fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="deco modal-header">
                <ul class="disp_error alert alert-danger list-unstyled" style="display: none;"></ul>
                <h5 class="modal-title" id="modal_title" style="position: absolute">社員詳細</h5>
                <h5 class="bt_change modal-title" data-dismiss="modal" style="margin:0 20px 0 0px;text-align: right;display: block"><span class="change_show1 bt_link">×</span></h5>
            </div>
            <div id="" style="display: block">
                <div class="modal-body">

                    <div class="modal-info">
                        <div class="modal-info-left">
                            社員名
                        </div>
                        <div class="modal-info-right">
                            大塚 翼
                        </div>
                        <div class="modal-info-left">
                            TEL
                        </div>
                        <div class="modal-info-right">
                            090-1111-2222
                        </div>
                        <div class="modal-info-left">
                            メールアドレス
                        </div>
                        <div class="modal-info-right">
                            t.ohtsuka@re-tech.net
                        </div>
                    </div>

                    <div class="deco form-group">
                        <label id="modal_label">担当案件履歴</label>
                        <table class="modal-table">
                            <tr>
                                <td>2018/12/24 13:00~17:00</td>
                                <td>田中様半日予約</td>
                                <td>前金請求中</td>
                            </tr>
                            <tr>
                                <td>2018/11/11 11:00~12:00</td>
                                <td>田中様1時間予約</td>
                                <td>利用済み</td>
                            </tr>
                            <tr>
                                <td>2018/9/30 9:00~18:00</td>
                                <td>田中様全日予約</td>
                                <td>利用済み</td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" class="mdl_uuid" value="">
                <button type="button" class="btn-edit btn btn-primary">編集</button>
            </div>
        </div>
    </div>
</div>
<!-- /Modal windoow -->
@endsection
@section('scripts')
<script type="text/javascript">

    (function($){

        $(document).on('click', '.tx-account_name', function(){
            $('#modal').modal('show');
        });

        $(document).on('click', '.btn-edit', function(){
            location.href = '/account/edit';
        });


        $('.fm_search').on('input', function(){
            var search_range = ['tx-account_name','tx-account_tel','tx-account_email'];
            searchWord(search_range);
        });

        $('#datatables').addClass('nowrap').dataTable({
            responsive: true,
            lengthChange: false,
            searching: false,
            ordering: false,
            info: false,
            paging: false,
            language: {
                "paginate" : {
                    "first" : "先頭",
                    "previous" : "前へ",
                    "next" : "次へ",
                    "last" : "末尾"
                }
            }
        });

    })(jQuery);

</script>
@endsection