@extends('template.master')
@section('contents')
<!-- Content area -->
<div class="row">
    <div class="col-lg-7">

        <div class="col-md-15">

            <!-- Basic layout-->
            <form action="/account/store" name="" id="" method="post">
                <div class="fm_account deco panel panel-flat">
                    <div class="panel-heading form_head">
                        @if(\Illuminate\Support\Facades\Session::has('flash_message_success'))
                            <ul class="validation alert alert-success list-unstyled">
                                <li>社員反映が完了しました。</li>
                            </ul>
                        @endif
                        @if(count($errors) > 0)
                            <ul class="validation alert alert-danger list-unstyled">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <h5 class="panel-title">{{ $page['title'] }}</h5>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <label>登録手順</label>
                            <div>
                                <a href="https://the-board.jp/user/invitation/new" target="_blank">Board</a>上でユーザー登録を完了させた後、下のボタンを押してください。
                            </div>
                        </div>
                        <div class="text-right">
                            {{ csrf_field() }}
                            <button type="submit" class="btn-submit btn btn-primary">登録反映</button>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /main charts -->
@endsection
@section('scripts')
<script type="text/javascript">

    (function($){

//        $(document).on('click', '.btn-submit', function(){
//            alert('登録処理');
//        });

    })(jQuery);
</script>
@endsection