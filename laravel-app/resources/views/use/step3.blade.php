@extends('template.master_use')
@section('contents')
<!-- Content area -->
<div class="row" style="margin: auto;">
    <div class="col-lg-7">
        <div class="use deco panel panel-flat">
            <div class="panel-heading form_head">
                <h5 class="panel-title">利用後項目チェック</h5>
                <div class="checkbox-area">
                    @foreach($termsOfEndService as $key => $term)
                    <label for="c1-{{ $key }}"><input type="checkbox" class="end_check" id="c1-{{ $key }}"> {{ $term }}</label>
                    @endforeach
                </div>
                <div class="comment-area form-group">
                    <label>コメント</label>
                    <textarea placeholder="特筆事項があれば記述。&#13;&#10;後から追加可能。" rows="5"></textarea>
                </div>
                <div class="penalty-area form-group">
                    <label>違約金有無</label>
                    <div class="checkbox-area">
                        <label for="c2-0"><input type="radio" class="penalty_check" name="penalty_check" id="c2-0" value="0" checked="checked"> 違約金が発生していません。</label>
                        <label for="c2-1"><input type="radio" class="penalty_check" name="penalty_check" id="c2-1" value="1"> 違約金が発生している可能性があります。</label>
                    </div>
                </div>
                <div class="btn-area">
                    <input type="hidden" class="project_uuid" value="{{ $project->project_uuid }}">
                    <button type="button" class="btn-submit btn btn-primary">確認しました。</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /main charts -->

<!-- Modal windoow -->
<!-- /Modal windoow -->
@endsection
@section('scripts')
<script type="text/javascript">

    (function($){

        $(document).on('click', '.btn-submit', function(){

            if ( $('.end_check:not(:checked)').size() ==0 ) {
                if($('.penalty_check:checked').val()=='0'){
                    endUse(0)
                }
                else if($('.penalty_check:checked').val()=='1'){
                    endUse(1)
                }
                else {
                    console.log('fatal error');
                    return false;
                }
            }
            else{
                alert("すべての項目を確認してください。");
            }

        });

    })(jQuery);

    function endUse(is_penalty)
    {
        var data = {
            uuid: $('.project_uuid').val(),
            is_penalty: is_penalty
        };

        $.ajax({
            url: '/use/endUse',
            type: "post",
            contentType: 'application/json',
            data: JSON.stringify(data),
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR){
            if(data.status == 1)
            {
                // OK
                if(is_penalty == 0)
                {
                    alert('利用を終了しました。\nお疲れ様でした。');
                }
                else
                {
                    alert('利用を終了しました。\n違約金の処理を後ほど操作してください。\nお疲れ様でした。');
                }
                location.href = '/';
            }
            else
            {
                // NG
                alert('fatal error');
            }
        }).fail(function(data, textStatus, errorThrown){
            alert('connection error');
        });
    }

</script>
@endsection