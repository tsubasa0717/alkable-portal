@extends('template.master_use')
@section('contents')
<!-- Content area -->
<div class="row" style="margin: auto;">
    <div class="col-lg-7">
        <div class="use deco panel panel-flat">
            <div class="panel-heading form_head">
                <h5 class="panel-title">延長確認</h5>
                <div class="checkbox-area">
                        <label for="0"><input type="radio" class="extension_check" name="extension_check" id="0" value="0"> 利用を終了します。</label>
                        <label for="1"><input type="radio" class="extension_check" name="extension_check" id="1" value="1"> 利用時間を延長します。</label>
                </div>
                <div class="select-time-area" style="display:none;">
                    <div class="form-group">
                        <label>延長時間選択</label>
                        <div class="cp_ipselect cp_sl01">
                            <select class="select-time" name="select-time">
                                <option value="0">延長時間選択</option>
                                <option value="1">1時間</option>
                                <option value="2">2時間</option>
                                <option value="3">3時間</option>
                                <option value="4">4時間</option>
                                <option value="5">5時間</option>
                                <option value="6">6時間</option>
                                <option value="7">7時間</option>
                                <option value="8">8時間</option>
                                <option value="9">9時間</option>
                            </select>
                            <input type="hidden" class="is_holiday" value="{{ $is_holiday }}">
                            <input type="hidden" class="fee_holiday" value="{{ env('HolidayFeePerHour') }}">
                            <input type="hidden" class="fee_weekday" value="{{ env('WeekdayFeePerHour') }}">
                            <input type="hidden" class="rate_tax" value="{{ env('ConsumptionTaxFee') }}">
                            <input type="hidden" class="end_hour" value="{{ \Carbon\Carbon::parse($project->termination_time)->hour }}">
                            <input type="hidden" class="end_minute" value="00">
                        </div>
                        <div class="calc-fee-area" style="display: none"></div>
                    </div>
                </div>
                <div class="btn-area">
                    <input type="hidden" class="project_uuid" value="{{ $project->project_uuid }}">
                    <button type="button" class="btn-submit btn btn-primary">送信</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /main charts -->

<!-- Modal windoow -->
<!-- /Modal windoow -->
@endsection
@section('scripts')
<script type="text/javascript">

    (function($){

        $(document).on('change', 'input[name=extension_check]', function(){
            if($(this).val()==0)
            {
                $('.select-time-area').slideUp();
                $('.calc-fee-area').slideUp();
                $('.select-time').val(0);
            }
            else
            {
                $('.select-time-area').slideDown();
            }

        });

        $(document).on('change', '.select-time', function() {
            if($('.select-time').val()!=0)
            {
                var fee = viewAmount(calcFee($('.select-time').val()));

                $('.calc-fee-area').empty();
                $('.calc-fee-area').append('利用料金：'+ fee +'円');
                $('.calc-fee-area').slideDown();
            }
            else
            {
                $('.calc-fee-area').slideUp();
            }
        });

        $(document).on('click', '.btn-submit', function(){

            if($('input[name=extension_check]:checked').val()=='0')
            {
                if(confirm('以降の延長は出来ません。\nよろしければ「はい」を押してください。'))
                {
                    endStandbyUse();
                }
            }
            else if($('input[name=extension_check]:checked').val()=='1')
            {
                if($('.select-time').val()!=0)
                {
                    checkExtension(Number($('.select-time').val()));
                }
                else
                {
                    alert('延長時間を選択してください。');
                }
            }
            else
            {
                alert('延長の有無をを選択してください。');
            }
        });

    })(jQuery);

    function endStandbyUse()
    {
        var data = {
            uuid: $('.project_uuid').val()
        };

        $.ajax({
            url: '/use/endStandbyUse',
            type: "post",
            contentType: 'application/json',
            data: JSON.stringify(data),
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR){
            if(data.status == 1)
            {
                // OK
                location.reload();
            }
            else
            {
                // NG
                alert('fatal error');
            }
        }).fail(function(data, textStatus, errorThrown){
            alert('connection error');
        });
    }

    function checkExtension(extension_time)
    {
        var data = {
            uuid: $('.project_uuid').val(),
            extension_time: extension_time
        };

        $.ajax({
            url: '/use/checkExtension',
            type: "post",
            contentType: 'application/json',
            data: JSON.stringify(data),
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR){
            console.log(data);
            if(data.status == 1)
            {
                // OK
                var fee = calcFee($('.select-time').val());

                if(confirm('延長可能の確認が出来ました。\n延長料金'+ fee +'円が支払われたら「OK」を押してください。'))
                {
                    // ここに延長処理
                    doExtension(extension_time,fee);
                }
            }
            else if(data.status == -1)
            {
                alert(data.message);
            }
            else
            {
                // NG
                alert('fatal error');
            }
        }).fail(function(data, textStatus, errorThrown){
            alert('connection error');
        });
    }

    function doExtension(extension_time,fee)
    {
        var data = {
            uuid: $('.project_uuid').val(),
            extension_time: extension_time,
            fee: fee
        };

        $.ajax({
            url: '/use/doExtension',
            type: "post",
            contentType: 'application/json',
            data: JSON.stringify(data),
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR){
            console.log(data);
            if(data.status == 1)
            {
                // OK
                var endTime = (Number($('.end_hour').val())+Number($('.select-time').val())) +':'+ $('.end_minute').val();
                alert($('.select-time').val()+'時間の延長を行いました。\n利用終了時間は'+endTime+'です。');
                location.href = '/';
            }
            else if(data.status == -1)
            {
                alert(data.message);
            }
            else
            {
                // NG
                alert('fatal error');
            }
        }).fail(function(data, textStatus, errorThrown){
            alert('connection error');
        });
    }

    function calcFee(time)
    {
        var isHoliday = $('.is_holiday').val();
        var feeHoliday = $('.fee_holiday').val();
        var feeWeekday = $('.fee_weekday').val();
        var rateTax = 1 + 0.01 * $('.rate_tax').val();

        if(isHoliday=='0')
        {
            fee = feeHoliday * time * rateTax;
        }
        else
        {
            fee = feeWeekday * time * rateTax;
        }

        return Math.round(fee);
    }

    function viewAmount(amount)
    {
        return String(amount).replace( /(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
    }


</script>
@endsection