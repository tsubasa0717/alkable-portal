@extends('template.master_use')
@section('contents')
<!-- Content area -->
<div class="row" style="margin: auto;">
    <div class="col-lg-7">
        <div class="use deco panel panel-flat">
            <div class="panel-heading form_head">
                <h5 class="panel-title">利用規約チェック</h5>
                <div class="checkbox-area">
                    @foreach($termsOfService as $key => $term)
                    <label for="{{ $key }}"><input type="checkbox" class="rule_check" id="{{ $key }}"> {{ $term }}</label>
                    @endforeach
                </div>
                <div class="btn-area">
                    <input type="hidden" class="project_uuid" value="{{ $project->project_uuid }}">
                    <input type="hidden" class="termination_time" value="{{ $project->termination_time }}">
                    <button type="button" class="btn-submit btn btn-primary">確認しました。</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /main charts -->

<!-- Modal windoow -->
<!-- /Modal windoow -->
@endsection
@section('scripts')
<script type="text/javascript">

    (function($){

        $(document).on('click', '.btn-submit', function(){
            if ( $('.rule_check:not(:checked)').size() == 0 ) {
               if(confirm('利用を開始します。\nよろしければ「はい」を押してください。')){
                    startUse();
               }
            }
            else{
                alert("すべての利用規約に同意してください。");
            }
        });
//           $('#modal').modal('show');

    })(jQuery);

    function startUse()
    {
        var data = {
            uuid: $('.project_uuid').val()
        };

        $.ajax({
            url: '/use/startUse',
            type: "post",
            contentType: 'application/json',
            data: JSON.stringify(data),
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR){
            if(data.status == 1)
            {
                // OK
                alert('利用を開始しました。\n利用終了時刻は' + $('.termination_time').val() + 'です。');
                location.href = '/';
            }
            else
            {
                // NG
                alert('fatal error');
            }
        }).fail(function(data, textStatus, errorThrown){
            alert('connection error');
        });
    }

</script>
@endsection