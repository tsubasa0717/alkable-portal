@extends('template.master_login')
@section('contents')
    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text responsive_login">
                    <img src="/assets/images/logo.png" alt="">
                    <div class="description">
                        <p>
                            ギャラリーあるかぶる案件ポータル
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 form-box" style="margin-top: 15px;">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>ログイン</h3>
                            <p>登録アドレスとパスワードを入力してください。</p>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-key"></i>
                        </div>
                    </div>
                    <div class="form-bottom">
                        @if ($errors->has('email'))
                            <ul class="validation alert alert-danger list-unstyled">
                                <li>{{ $errors->first('email') }}</li>
                            </ul>
                        @endif
                        <form role="form" class="login-form" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="sr-only" for="form-username">メールアドレス</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="メールアドレス" required autofocus />
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-password">パスワード</label>
                                <input type="password" name="password" placeholder="パスワード" class="form-control" id="password" required />
                            </div>
                            <button type="submit" class="btn create_btn">ログイン</button>
                            <div style="text-align: right">
                                {{--パスワードを忘れた方は<a href="{{ route('password.request') }}">こちら</a>--}}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>

        (function($){

        })(jQuery);

    </script>
@endsection