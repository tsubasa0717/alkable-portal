@extends('template.master')
@section('contents')
<!-- Content area -->
<div class="row">
    <div class="col-lg-7">
        <div class="col-md-15">

            <!-- Basic layout-->
            <form action="/invoice/store" name="" id="" method="post">
                <div class="fm_project deco panel panel-flat">
                    <div class="panel-heading form_head">
                        @if(\Illuminate\Support\Facades\Session::has('flash_message_success'))
                            <ul class="validation alert alert-success list-unstyled">
                                <li>{{ \Illuminate\Support\Facades\Session::get('invoice-name') }}の登録が完了しました。</li>
                            </ul>
                        @endif
                        @if(count($errors) > 0)
                            <ul class="validation alert alert-danger list-unstyled">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <h5 class="panel-title">{{ $page['title'] }}</h5>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label" for="">プロジェクト選択</label>
                            <select data-placeholder="" name="project_id" class="chosen-select select-project-id form-control" id="">
                                <option value="">-- 選択してください --</option>
                                @foreach($projects as $key => $project)
                                <option value="{{ $project->project_id }}" @if(old('project_id',$form['project_id']==$project->project_id)) selected @endif>{{ $project->project_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="">請求タイプ</label>
                            <select name="invoice_type" class="select-invoice-type form-control" id="">
                                <option value="">-- 請求タイプを選択してください --</option>
                                <option value="1" @if(old('invoice_type',$form['invoice_type']==1)) selected @endif>利用延長料</option>
                                <option value="2" @if(old('invoice_type',$form['invoice_type']==2)) selected @endif>違約金</option>
                                <option value="3" @if(old('invoice_type',$form['invoice_type']==3)) selected @endif>その他</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>請求金額</label>
                            <input type="number" name="invoice_amount" class="invoice-amount form-control" placeholder="数字のみ入力してください。" value="{{ old('invoice_amount',$form['invoice_amount']) }}" required />
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="">料金受付締切日選択</label>
                            <fieldset class="fieldset">
                                <input name="invoice_deadline" class="select-deadline-day form-control" id="select-deadline-day" type="text" value="{{ old('invoice_deadline',$form['invoice_deadline']) }}">
                            </fieldset>
                        </div>

                        <div class="text-right">
                            {{ csrf_field() }}
                            <button type="submit" class="btn-submit btn btn-primary">登録</button>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /main charts -->

<!-- Modal windoow -->
<div id="modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>
<!-- /Modal windoow -->
@endsection
@section('scripts')
<script type="text/javascript">

    (function($){

        $('#select-deadline-day').pickadate();

    })(jQuery);
</script>
@endsection