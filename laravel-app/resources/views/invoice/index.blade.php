@extends('template.master')
@section('contents')
<!-- Content area -->
<div class="row">
    <div class="col-lg-7">
        <div class="panel panel-success">
            <div class="panel-heading">
                <div style="position:absolute;">{{ $page['title'] }}</div>
                <div style="text-align: right"><span class="responsive-display-inline">検索： </span><input class="fm_search" type="text" placeholder="検索ワード" style="height:20px"></div>
            </div>
            <table id="datatables" class="table table-striped table-bordered table-hover table-bill">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>請求内容</th>
                    <th>金額</th>
                    <th>ステータス</th>
                    <th>支払い期間</th>
                    <th></th>
                </tr>
                </thead>
                <tbody id="sort_table">
                @foreach($invoices as $key => $invoice)
                <tr class="list_tr">
                    <td>{{ $key+1 }}</td>
                    <td class="tx-bill_type tx_link" data-uuid="{{ $invoice->invoice_uuid }}">{{ $invoice->invoice_name }}</td>
                    <td class="tx-bill_amount">{{ $invoice->invoice_amount }}円</td>
                    <td class="tx-bill_status">{{ $invoice->invoice_status_name }}</td>
                    <td class="tx-bill-deadline">~{{ $invoice->invoice_deadline }} {!! $invoice->invoice_remain_deadline !!}</td>
                    <td><button type="button" class="btn-edit btn btn-primary" data-uuid="{{ $invoice->invoice_uuid }}">編集</button></td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="responsive-display heading-text pull-right" style="margin-top:-10px;margin-bottom:50px;padding-right:10px;"><a href="/invoice/create"><img class="btn-img" src="/assets/img/btn/plus.png" width="50px" height="50px"></a></div>
        <input type="hidden" class="invoice_uuid" value="{{ $invoice_uuid }}">
    </div>
</div>
<!-- /main charts -->

<!-- Modal windoow -->
<div id="modal" class="modal modal-bill fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="deco modal-header">
                <ul class="disp_error alert alert-danger list-unstyled" style="display: none;"></ul>
                <h5 class="modal-title" id="modal_title" style="position: absolute">請求詳細</h5>
                <h5 class="bt_change modal-title" data-dismiss="modal" style="margin:0 20px 0 0px;text-align: right;display: block"><span class="change_show1 bt_link">×</span></h5>
            </div>
            <div id="" style="display: block">
                <div class="modal-body">

                    <div class="modal-info">
                        <div class="modal-info-left">
                            請求内容
                        </div>
                        <div class="modal-info-right">
                            <span class="mdl-invoice_title"></span>
                        </div>
                        <div class="modal-info-left">
                            金額
                        </div>
                        <div class="modal-info-right">
                            <span class="mdl-invoice_amount"></span><span class="mdl-invoice_remain_amount"></span>
                        </div>
                        <div class="modal-info-left">
                            状態
                        </div>
                        <div class="modal-info-right">
                            <span class="mdl-invoice_status_name"></span>
                        </div>
                        <div class="modal-info-left">
                            受付期間
                        </div>
                        <div class="modal-info-right">
                            <span class="mdl-invoice_deadline"></span>
                        </div>
                        <div class="modal-info-left">
                            該当プロジェクト名
                        </div>
                        <div class="modal-info-right">
                            <span class="mdl-invoice_project_name"></span>
                        </div>
                        <div class="modal-info-left">
                            該当顧客名
                        </div>
                        <div class="modal-info-right">
                            <span class="mdl-invoice_client_name"></span>
                        </div>

                    </div>

                    <div class="btn-doc-area">
                        <button type="button" class="btn-quotation btn btn-success">見積書</button>
                        <button type="button" class="btn-invoice btn btn-primary">請求書</button>
                        <button type="button" class="btn-receipt btn btn-warning">領収書</button>
                    </div>

                    <div class="pay-area">
                        <div class="inline-block">【支払い処理】</div>
                        <div class="fm-pay-area inline-block"><input name="payment_amount" class="fm_pay payment_amount" type="text" placeholder="" style="height:20px"> 円</div>
                        <div class="inline-block"><button type="button" class="mdl-btn-payment btn-pay btn btn-danger" data-uuid="">支払う</button></div>
                    </div>

                    <div class="deco form-group">
                        <label id="modal_label">支払い履歴</label>
                        <table class="modal-table modal-payments-table">
                        </table>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" class="mdl_uuid" value="">
                <button type="button" class="btn-edit mdl-btn-edit btn btn-primary" data-uuid="">編集</button>
            </div>
        </div>
    </div>
</div>
<!-- /Modal windoow -->
@endsection
@section('scripts')
<script type="text/javascript">

    (function($){

        if($('.invoice_uuid').val() != '')
        {
            showInvoiceData($('.invoice_uuid').val());
        }

        $(document).on('click', '.tx-bill_type', function(){
            var uuid = $(this).attr('data-uuid');
            showInvoiceData(uuid);
        });

        $(document).on('click', '.btn-quotation', function(){
            alert('見積書発行処理');
        });

        $(document).on('click', '.btn-invoice', function(){
            alert('請求書発行処理');
        });

        $(document).on('click', '.btn-receipt', function(){
            alert('領収書発行処理');
        });

        $(document).on('click', '.btn-pay', function(){

            var pay_amount = Number($('.fm_pay').val());
            var uuid = $(this).attr('data-uuid');

            if(pay_amount!=NaN && Number.isInteger(pay_amount) && pay_amount>0)
            {
                if(confirm(pay_amount+'円の支払いを確定します。よろしければOKを押してください。'))
                {
                    runPayment(uuid,pay_amount);
                }
                else{
                    return true;
                }
            }
            else{
                alert('正しい金額を入力して下さい。');
            }
        });

        $(document).on('click', '.btn-edit', function(){
            var uuid = $(this).attr('data-uuid');
            location.href = '/invoice/edit?uuid=' + uuid;
        });

        $('.fm_search').on('input', function(){
            var search_range = ['tx-bill_type','tx-bill_amount','tx-bill_status','tx-bill-deadline'];
            searchWord(search_range);
        });

        $('#datatables').addClass('nowrap').dataTable({
            responsive: true,
            lengthChange: false,
            searching: false,
            ordering: false,
            info: false,
            paging: false,
            language: {
                "paginate" : {
                    "first" : "先頭",
                    "previous" : "前へ",
                    "next" : "次へ",
                    "last" : "末尾"
                }
            }
        });

    })(jQuery);

    function showInvoiceData(uuid)
    {
        var data = {
            uuid: uuid
        };

        $.ajax({
            url: '/invoice/show',
            type: "post",
            contentType: 'application/json',
            data: JSON.stringify(data),
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR){
            if(data.status == 1)
            {
                // OK
                resetModal();
                console.log(data.view_data.payments);
                $('.mdl-invoice_title').text(data.view_data.invoice.invoice_name);
                $('.mdl-invoice_amount').text(data.view_data.invoice.invoice_amount + '円');

                if(data.view_data.invoice.invoice_amount != data.view_data.invoice.invoice_remain_amount && data.view_data.invoice.invoice_remain_amount != 0)
                {
                    $('.mdl-invoice_remain_amount').text(' (残り'+data.view_data.invoice.invoice_remain_amount+'円)');
                }

                $('.mdl-invoice_status_name').text(data.view_data.invoice.invoice_view_status_name);
                $('.mdl-invoice_deadline').text('~' + data.view_data.invoice.invoice_deadline);
                $('.mdl-invoice_project_name').text(data.view_data.invoice.invoice_project_name);
                $('.mdl-invoice_client_name').text(data.view_data.invoice.invoice_client_name);
                $('.payment_amount').attr('placeholder',data.view_data.invoice.invoice_remain_amount);
                $('.mdl-btn-payment').attr('data-uuid',data.view_data.invoice.invoice_uuid);
                $('.mdl-btn-edit').attr('data-uuid',data.view_data.invoice.invoice_uuid);
                if(data.view_data.payments.length == 0)
                {
                    $('.modal-payments-table').prepend(
                        $('<tr></tr>')
                            .append($('<td></td>').text('支払い履歴はありません。'))
                    );
                }
                else
                {
                    var remain_amount = data.view_data.invoice.invoice_amount;
                    for(var i=0;i<data.view_data.payments.length;i++)
                    {
                        remain_amount = remain_amount - data.view_data.payments[i].payment_amount;
                        var time_text = generateTimeText(data.view_data.payments[i].created_at);
                        $('.modal-payments-table').prepend(
                            $('<tr></tr>')
                                .append($('<td></td>').text(time_text))
                                .append($('<td></td>').text(data.view_data.payments[i].payment_amount + '円'))
                                .append($('<td></td>').text('残り'+remain_amount+'円'))
                        );
                    }
                }

                $('#modal').modal('show');
            }
            else
            {
                // NG
                alert('fatal error');
            }
        }).fail(function(data, textStatus, errorThrown){
            alert('connection error');
        });
    }

    function resetModal()
    {
        $('.mdl-invoice_title').text('');
        $('.mdl-invoice_amount').text('');
        $('.mdl-invoice_remain_amount').text('');
        $('.mdl-invoice_status_name').text('');
        $('.mdl-invoice_deadline').text('');
        $('.mdl-invoice_project_name').text('');
        $('.mdl-invoice_client_name').text('');
        $('.payment_amount').val('');
        $('.payment_amount').attr('placeholder','');
        $('.mdl-btn-edit').attr('data-uuid','');
        $('.modal-payments-table').empty();
    }

    function runPayment(uuid,payment_amount)
    {
        var data = {
            uuid: uuid,
            payment_amount: payment_amount
        };

        $.ajax({
            url: '/payment',
            type: "post",
            contentType: 'application/json',
            data: JSON.stringify(data),
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR){
            if(data.status == 1)
            {
                // OK
                alert(payment_amount + '円の支払いを確定しました。');
                $('#modal').modal('hide');
                location.href = '/invoice?uuid=' + uuid;
            }
            else
            {
                // NG
                alert(data.message);
            }
        }).fail(function(data, textStatus, errorThrown){
            alert('connection error');
        });
    }

    function generateTimeText(timeStamp)
    {
        var timeText = moment(timeStamp).format('YYYY/MM/DD');

        return timeText;
    }

</script>
@endsection