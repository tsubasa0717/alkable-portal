@extends('template.master')
@section('contents')
<!-- Content area -->
<div class="row">
    <div class="col-lg-7">
        <div class="col-md-15">

            <!-- Basic layout-->
            <form action="/client/store" name="" id="" method="post">
                <div class="fm_project deco panel panel-flat">
                    <div class="panel-heading form_head">
                        @if(\Illuminate\Support\Facades\Session::has('flash_message_success'))
                            <ul class="validation alert alert-success list-unstyled">
                                <li>{{ \Illuminate\Support\Facades\Session::get('client-name') }}の登録が完了しました。</li>
                            </ul>
                        @endif
                        @if(count($errors) > 0)
                            <ul class="validation alert alert-danger list-unstyled">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <h5 class="panel-title">{{ $page['title'] }}</h5>
                    </div>

                    <div class="panel-body">

                        <div class="form-group">
                            <label>顧客名</label>
                            <input type="text" name="client_name" class="client-name form-control" placeholder="" value="{{ old('client_name',$form['client_name']) }}" required />
                        </div>

                        <div class="form-group">
                            <label>TEL</label>
                            <input type="text" name="client_tel" class="client-tel form-control" placeholder="090-0000-0000" value="{{ old('client_tel',$form['client_tel']) }}" />
                        </div>

                        <div class="form-group">
                            <label>メールアドレス</label>
                            <input type="text" name="client_email" class="client-email form-control" placeholder="gallery@alkable.com" value="{{ old('client_email',$form['client_email']) }}" />
                        </div>

                        <div class="form-group">
                            <label>コメント</label>
                            <textarea name="client_comment" class="client-comment form-control" placeholder="備考があればここに記入">{{ old('client_comment',$form['client_comment']) }}</textarea>
                        </div>

                        <div class="text-right">
                            {{ csrf_field() }}
                            <input type="hidden" name="is_black" value="{{ old('is_black',$form['is_black']) }}">
                            <input type="hidden" name="client_uuid" value="{{ old('client_uuid',$form['client_uuid']) }}">
                            <button type="submit" class="btn-submit btn btn-primary">登録</button>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /main charts -->

<!-- Modal windoow -->
<div id="modal" class="modal fade">
</div>
<!-- /Modal windoow -->
@endsection
@section('scripts')
<script type="text/javascript">

    (function($){

    })(jQuery);

</script>
@endsection