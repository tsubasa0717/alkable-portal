@extends('template.master')
@section('contents')
<!-- Content area -->
<div class="row">
    <div class="col-lg-7">
        <div class="panel panel-success">
            <div class="panel-heading">
                <div style="position:absolute;">{{ $page['title'] }}</div>
                <div style="text-align: right"><span class="responsive-display-inline">検索： </span><input class="fm_search" type="text" placeholder="検索ワード" style="height:20px"></div>
            </div>

            <table id="datatables" class="table table-striped table-bordered table-hover table-client">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>顧客名</th>
                    <th>TEL</th>
                    <th>最終利用日</th>
                    <th></th>
                </tr>
                </thead>
                <tbody id="sort_table">
                @foreach($clients as $key => $client)
                <tr class="list_tr">
                    <td>{{ $key+1 }}</td>
                    <td class="tx-client_name tx_link" data-uuid="{{ $client->client_uuid }}">@if($client->is_black==1)<span class="black-icon"></span>@endif{{ $client->client_name }}</td>
                    <td class="tx-client_tel">{{ $client->client_tel }}</td>
                    <td class="tx-client_utilization_time">{{ $client->last_use_day }}</td>
                    <td><button type="button" class="btn_edit btn btn-primary" data-uuid="{{ $client->client_uuid }}">編集</button></td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="responsive-display heading-text pull-right" style="margin-top:-10px;margin-bottom:50px;padding-right:10px;"><a href="/client/create"><img src="/assets/img/btn/plus.png" class="btn-img" width="50px" height="50px"></a></div>
    </div>
</div>
<!-- /main charts -->

<!-- Modal windoow -->
<div id="modal" class="modal modal-client fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="deco modal-header">
                <ul class="disp_error alert alert-danger list-unstyled" style="display: none;"></ul>
                <h5 class="modal-title" id="modal_title" style="position: absolute">顧客詳細</h5>
                <h5 class="bt_change modal-title" data-dismiss="modal" style="margin:0 20px 0 0px;text-align: right;display: block"><span class="change_show1 bt_link">×</span></h5>
            </div>
            <div id="" style="display: block">
                <div class="modal-body">

                    <div class="modal-info">
                        <div class="modal-info-left">
                            顧客名
                        </div>
                        <div class="modal-info-right">
                            <span class="mdl-client-name"></span>
                        </div>
                        <div class="modal-info-left">
                            TEL
                        </div>
                        <div class="modal-info-right">
                            <span class="mdl-client-tel"></span>
                        </div>
                        <div class="modal-info-left">
                            メールアドレス
                        </div>
                        <div class="modal-info-right">
                            <span class="mdl-client-email"></span>
                        </div>
                        <div class="modal-info-left">
                            利用回数
                        </div>
                        <div class="modal-info-right">
                            <span class="mdl-client-use-times"></span>
                        </div>
                        <div class="modal-info-left">
                            合計利用時間
                        </div>
                        <div class="modal-info-right">
                            <span class="mdl-client-summary-time"></span>
                        </div>
                    </div>

                    <div class="deco form-group">
                        <label id="modal_label">コメント</label>
                        <div class="fm_text"><span class="mdl-client-comment"></span></div>
                    </div>

                    <div class="deco form-group">
                        <label id="modal_label">利用履歴</label>
                        <table class="modal-table modal-project-table">
                        </table>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" class="mdl_uuid" value="">
                <button type="button" class="btn_edit btn btn-primary">編集</button>
            </div>
        </div>
    </div>
</div>
<!-- /Modal windoow -->
@endsection
@section('scripts')
<script type="text/javascript">

    (function($){

        $(document).on('click', '.tx-client_name', function(){
            var uuid = $(this).attr('data-uuid');
            showClientData(uuid);
        });

        $(document).on('click', '.btn_edit', function(){
            var uuid = $(this).attr('data-uuid');
            location.href = '/client/edit?uuid=' + uuid;
        });

        $('.fm_search').on('input', function(){
            var search_range = ['tx-client_name','tx-client_tel','tx-client_utilization_time'];
            searchWord(search_range);
        });

        $('#datatables').addClass('nowrap').dataTable({
            responsive: true,
            lengthChange: false,
            searching: false,
            ordering: false,
            info: false,
            paging: false,
            language: {
                "paginate" : {
                    "first" : "先頭",
                    "previous" : "前へ",
                    "next" : "次へ",
                    "last" : "末尾"
                }
            }
        });

    })(jQuery);

    function showClientData(uuid)
    {
        var data = {
            uuid: uuid
        };

        $.ajax({
            url: '/client/show',
            type: "post",
            contentType: 'application/json',
            data: JSON.stringify(data),
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR){
            if(data.status == 1)
            {
                // OK
                resetModal();
                console.log(data.view_data.client);
                $('.mdl-client-name').text(data.view_data.client.client_name);
                $('.mdl-client-tel').text(data.view_data.client.client_tel);
                $('.mdl-client-email').text(data.view_data.client.client_email);
                $('.mdl-client-use-times').text(data.view_data.client.client_use_times + '回');
                $('.mdl-client-summary-time').text(data.view_data.client.client_summary_time + '時間');
                $('.mdl-client-comment').html(data.view_data.client.client_comment);
                if(data.view_data.client.client_projects.length == 0)
                {
                    $('.modal-project-table').append(
                        $('<tr></tr>')
                            .append($('<td></td>').text('利用履歴はありません。'))
                    );
                }
                else
                {
                    for(var i=0;i<data.view_data.client.client_projects.length;i++)
                    {
                        var time_text = generateTimeText(data.view_data.client.client_projects[i]);
                        $('.modal-project-table').append(
                            $('<tr></tr>')
                                .append($('<td></td>').text(time_text))
                                .append($('<td></td>').text(data.view_data.client.client_projects[i].project_name))
                                .append($('<td></td>').text(data.view_data.client.client_projects[i].reservation_status_name))
                        );
                    }
                }

                $('#modal').modal('show');
            }
            else
            {
                // NG
                alert('fatal error');
            }
        }).fail(function(data, textStatus, errorThrown){
            alert('connection error');
        });
    }

    function generateTimeText(project)
    {
        var start_time = moment(project.start_time).format('YYYY/MM/DD HH:mm');
        var end_time = moment(project.termination_time).format('HH:mm');


        return start_time + '~' + end_time;
    }

    function resetModal()
    {
        $('.mdl-client-name').text('');
        $('.mdl-client-tel').text('');
        $('.mdl-client-email').text('');
        $('.mdl-client-use-times').text('');
        $('.mdl-client-summary-time').text('');
        $('.mdl-client-comment').text('');
        $('.modal-project-table').empty();
    }

</script>
@endsection