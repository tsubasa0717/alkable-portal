@extends('template.master')
@section('contents')
    <!-- Content area -->
    <div class="row">
        <div class="col-lg-7">
            <div class="deco panel panel-flat">
                <div class="panel-heading form_head deco">
                    <h5 class="panel-title">{{ $project->project_name }}</h5>
                    <input type="hidden" class="project_uuid" value="{{ $project->project_uuid }}">
                </div>
                <div class="panel-body deco">
                    <div class="deco form-group">
                        <div class="project-info">
                            <div class="project-info-left">
                                顧客名
                            </div>
                            <div class="project-info-right">
                                {{ $project->client_name }}
                            </div>
                            <div class="project-info-left">
                                利用日
                            </div>
                            <div class="project-info-right">
                                {{ $project->use_day }}
                            </div>
                            <div class="project-info-left">
                                利用時間
                            </div>
                            <div class="project-info-right">
                                {{ $project->start_time }} ~ {{ $project->termination_time }}
                            </div>
                            <div class="project-info-left">
                                担当者
                            </div>
                            <div class="project-info-right">
                                {{ $project->account_name }}
                            </div>
                            <div class="project-info-left">
                                予約ステータス
                            </div>
                            <div class="project-info-right">
                                {{ $project->reservation_status_name }}
                            </div>
                        </div>
                    </div>
                    <div class="deco form-group">
                        <label id="modal_label">コメント</label>
                        <div class="">
                            {{ $project->project_comment }}
                        </div>
                    </div>
                    <div class="deco form-group project-show-table">
                        <label id="modal_label">請求履歴</label>
                        <table class="project-show-table">
                            <tr>
                                <th>日時</th>
                                <th>状態</th>
                                <th>名前</th>
                                <th>金額</th>
                                <th>備考</th>
                                <th></th>
                            </tr>
                            @foreach($invoices as $key => $invoice)
                            <tr>
                                <td>{{ $invoice->invoice_day }}</td>
                                <td>{{ $invoice->invoice_status_name }}</td>
                                <td><span class="tx_link">{{ $invoice->invoice_name }}</span></td>
                                <td>{{ $invoice->invoice_amount }}円</td>
                                <td>{{ $invoice->invoice_comment }}</td>
                                <td>@if($invoice->invoice_status != 2)<button type="button" class="btn-payment btn btn-success" data-uuid="{{ $invoice->invoice_uuid }}">支払い</button>@endif</td>
                            </tr>
                            @endforeach
                            <tr class="btn-area">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td style="text-align: right;padding:0;"><img class="btn-invoice btn-img" src="/assets/img/btn/plus.png" width="25px" height="25px"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="text-right">
                        <button type="button" class="btn-edit btn btn-primary">編集</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /main charts -->

<!-- Modal windoow -->
<div id="modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>
<!-- /Modal windoow -->
@endsection
@section('scripts')
    <script type="text/javascript">

        (function($){

            var uuid = $('.project_uuid').val();

            $(document).on('click', '.btn-payment', function(){
                location.href = '/invoice?uuid=' + $(this).attr('data-uuid');
            });

            $(document).on('click', '.btn-invoice', function(){
                location.href = '/invoice/create?uuid=' + uuid;
            });

            $(document).on('click', '.btn-edit', function(){
                location.href = '/project/edit?uuid=' + uuid;
            });

        })(jQuery);

    </script>
@endsection