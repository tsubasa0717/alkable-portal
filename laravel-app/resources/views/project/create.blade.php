@extends('template.master')
@section('contents')
    <!-- Content area -->
    <div class="row">
        <div class="col-lg-7">
            <div class="col-md-15">

                <!-- Basic layout-->
                <form action="/project/store" name="" id="" method="post">
                    <div class="fm_project deco panel panel-flat">
                        <div class="panel-heading form_head">
                            @if(\Illuminate\Support\Facades\Session::has('flash_message_success'))
                                <ul class="validation alert alert-success list-unstyled">
                                    <li>{{ Session::get('project-name') }}の登録が完了しました。</li>
                                </ul>
                            @endif
                            @if(count($errors) > 0)
                                <ul class="validation alert alert-danger list-unstyled">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                            <h5 class="panel-title">{{ $page['title'] }}</h5>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label" for="">顧客名</label>
                                <div class="select-add-form-area">
                                    <div class="select-add-form-select-area">
                                        <select data-placeholder="" name="client_id" class="chosen-select select-client-id form-control" id="">
                                            <option value="0">-- 選択してください --</option>
                                            @foreach($clients as $key => $client)
                                            <option value="{{ $client->client_id }}" @if(old('client_id',$form['client_id'])==$client->client_id) selected @endif>{{ $client->client_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="select-add-form-add-area">
                                        <img class="btn-client-add btn-img" src="/assets/img/btn/plus.png" width="25px" height="25px">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label" for="sensor_category">担当者名</label>
                                <select name="account_id" class="form-control" id="">
                                    <option value="">-- 選択してください --</option>
                                    @foreach($accounts as $key => $account)
                                    <option value="{{ $account->account_id }}" @if(old('account_id',$form['account_id'])==$account->account_id) selected @endif>{{ $account->last_name }} {{ $account->first_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="control-label" for="">利用日選択</label>
                                <fieldset class="fieldset">
                                    <input name="use_day" class="select-use-day form-control" id="select-use-day" type="text" placeholder="ここをクリック" value="{{ old('use_day',$form['use_day']) }}">
                                </fieldset>
                                <div class="checkbox-area" style="display: none">
                                    <label for="0"><input type="radio" class="holiday_check" name="holiday_check" id="0" value="0"> 平日</label>
                                    <label for="1"><input type="radio" class="holiday_check" name="holiday_check" id="1" value="1"> 休日<span class="holiday_detail"></span></label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label" for="">利用時間選択</label>
                                <select name="start_hour" class="select-start-time form-control" id="">
                                    <option value="0">-- 開始時間を選択してください --</option>
                                    @for($i=10;$i<20;$i++)
                                        <option value="{{ $i }}" @if(old('start_hour',$form['start_hour'])==$i) selected @endif>{{ $i }}:00から</option>
                                    @endfor
                                </select>
                                <select name="end_hour" class="select-end-time form-control" id="">
                                    <option value="0">-- 終了時間を選択してください --</option>
                                    @for($i=11;$i<=20;$i++)
                                        <option value="{{ $i }}" @if(old('end_hour',$form['end_hour'])==$i) selected @endif>{{ $i }}:00まで</option>
                                    @endfor
                                </select>
                            </div>

                            <div class="form-group">
                                <label>利用料金(税抜き)</label>
                                <input type="number" name="project_amount" class="use-fee form-control" placeholder="自動入力されます。" value="{{ old('project_amount',$form['project_amount']) }}" required />
                                <input type="hidden" class="per-fee-weekday" value="{{ env('WeekdayFeePerHour') }}">
                                <input type="hidden" class="per-fee-holiday" value="{{ env('HolidayFeePerHour') }}">
                                <input type="hidden" class="all-fee-weekday" value="{{ env('WeekdayFeeAllDay') }}">
                                <input type="hidden" class="all-fee-holiday" value="{{ env('HolidayFeeAllDay') }}">
                            </div>

                            <div class="form-group">
                                <label class="control-label" for="">料金受付締切日選択</label>
                                <fieldset class="fieldset">
                                    <input name="invoice_deadline" class="select-deadline-day form-control" id="select-deadline-day" type="text" placeholder="自動選択されます。" value="{{ old('invoice_deadline',$form['invoice_deadline']) }}">
                                </fieldset>
                            </div>

                            <div class="form-group">
                                <label>プロジェクト名</label>
                                <input type="text" name="project_name" class="project-name form-control" placeholder="自動入力されます。" value="{{ old('project_name',$form['project_name']) }}" required />
                            </div>

                            <div class="form-group">
                                <label>コメント</label>
                                <textarea name="project_comment" class="project-comment form-control" placeholder="備考があればここに記入">{{ old('project_comment',$form['project_comment']) }}</textarea>
                            </div>

                            <div class="text-right">
                                {{ csrf_field() }}
                                <button type="submit" class="btn-submit btn btn-primary">登録</button>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /main charts -->

    <!-- Modal windoow -->
    <div id="modal" class="modal modal-project fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="deco modal-header">
                    <ul class="disp_error alert alert-danger list-unstyled" style="display: none;"></ul>
                    <h5 class="modal-title" id="modal_title" style="position: absolute">顧客登録</h5>
                    <h5 class="bt_change modal-title" data-dismiss="modal" style="margin:0 20px 0 0px;text-align: right;display: block"><span class="change_show1 bt_link">×</span></h5>
                </div>
                <div id="" style="display: block">
                    <div class="modal-body">
                        <div class="deco form-group">
                            <label>顧客名</label>
                            <input type="text" name="" class="tx-create-client-name form-control" placeholder="" value="" required />
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="hidden" class="mdl_uuid" value="">
                    <button type="button" class="btn-client-create btn btn-primary">登録</button>
                </div>

            </div>
        </div>
    </div>
    <!-- /Modal windoow -->
@endsection
@section('scripts')
    <script type="text/javascript">

        (function($){

            init();

            $(document).on('click', '.btn-client-add', function(){
                $('#modal').modal('show');
            });

            $(document).on('change', '.select-client-id', function(){
                insertProjectName();
            });

            $(document).on('change', '.select-start-time', function(){
                calcFee();
                insertProjectName();
            });

            $(document).on('change', '.select-end-time', function(){
                calcFee();
                insertProjectName();
            });

            $(document).on('change', '.holiday_check', function(){
                calcFee();
            });

            $(document).on('change', '.select-use-day', function(){

                var day = $(this).val();
                if($(this).val()!='')
                {
                    var useDate = formatDate($('.select-use-day').val());
                    var deadlineDay = calcDeadlineDay(useDate);
                    $('.select-deadline-day').val(deadlineDay);
                    $('.checkbox-area').slideUp();
                    checkHoliday(day);
                }
                else
                {
                    $('.select-start-time').val('0');
                    $('.select-end-time').val('0');
                    $('.select-end-time').val('0');
                    $('.select-deadline-day').val('');
                    $('.use-fee').val('');
                    $('.holiday_check[value=0]').prop('checked', true);
                    $('.holiday_detail').text('');
                    $('.checkbox-area').slideUp();
                }
            });

            $(document).on('click', '.btn-client-create', function(){
                var clientName = $('.tx-create-client-name').val();
                createClient(clientName);
            });

        })(jQuery);

        function init()
        {
            $('#select-use-day').pickadate();
            $('#select-deadline-day').pickadate();
            $('.chosen-select').chosen();
            $('.chosen-select-start-time').chosen({
                'inherit_select_classes': true
            });

            if($('.select-use-day').val()!='')
            {
                var day = $('.select-use-day').val();
                var useDate = formatDate($('.select-use-day').val());
                var deadlineDay = calcDeadlineDay(useDate);
                $('.select-deadline-day').val(deadlineDay);
                $('.checkbox-area').slideUp();
                checkHoliday(day);
            }
        }

        function calcFee()
        {

            if($('.select-start-time').val()=='0' || $('.select-end-time').val()=='0' || Number($('.select-end-time').val()) < Number($('.select-start-time').val()))
            {
                $('.use-fee').val('');
                return false;
            }

            // 平日
            var  useFee;
            if($('.holiday_check:checked').val()==0)
            {
                // 平日一日利用
                if($('.select-start-time').val()=='10' && $('.select-end-time').val()=='20')
                {
                    useFee = Number($('.all-fee-weekday').val());
                }
                // 平日時間利用
                else
                {
                    useFee = (Number($('.select-end-time').val()) - Number($('.select-start-time').val()))*Number($('.per-fee-weekday').val());
                }
            }
            else if($('.holiday_check:checked').val()==1)
            {
                // 休日一日利用
                if($('.select-start-time').val()=='10' && $('.select-end-time').val()=='20')
                {
                    useFee = Number($('.all-fee-holiday').val());
                }
                // 休日時間利用
                else
                {
                    useFee = (Number($('.select-end-time').val()) - Number($('.select-start-time').val()))*Number($('.per-fee-holiday').val());
                }
            }
            else
            {
                return false;
            }

            $('.use-fee').val(useFee);

        }

        function calcDeadlineDay(formatDate)
        {
            var useDay = moment(formatDate);
            var nextWeekDate = moment(formatDate).add(1, "weeks");
            var deadlineDay;

            if (useDay.diff(nextWeekDate, 'days') < 0)
            {
                deadlineDay = useDay.format("YYYY年MM月DD日");
            }
            else
            {
                deadlineDay = nextWeekDate.format("YYYY年MM月DD日");
            }

            return deadlineDay;
        }

        function formatDate(date)
        {
            date = date.replace("年", "-").replace("月", "-").replace("日", "");
            return date;
        }

        function insertProjectName()
        {
            var clientName = $('.select-client-id option:selected').text();
            var startTime = Number($('.select-start-time').val());
            var endTime = Number($('.select-end-time').val());

            if($('.select-client-id option:selected').val() == '0' || startTime == '0' || endTime == '0')
            {
                return false;
            }

            var timeText;
            if($('.select-start-time').val()=='10' && $('.select-end-time').val()=='20')
            {
                timeText = '全日';
            }
            else
            {
                timeText = (endTime-startTime) + '時間';
            }

            $('.project-name').val(clientName + '様' + timeText + '利用')
        }

        function checkHoliday(day)
        {
            var data = {
                day: day
            };

            $.ajax({
                url: '/project/checkHoliday',
                type: "post",
                contentType: 'application/json',
                data: JSON.stringify(data),
                dataType: 'json'
            }).done(function(data, textStatus, jqXHR){
                if(data.status == 1)
                {
                    // OK
                    var result = data.view_data;
                    if(result.status)
                    {
                        $('.holiday_check[value=1]').prop('checked', true);
                        $('.holiday_detail').text('('+result.detail+')');
                    }
                    else
                    {
                        $('.holiday_check[value=0]').prop('checked', true);
                        $('.holiday_detail').text('');
                    }
                    calcFee();
                    $('.checkbox-area').slideDown();
                }
                else
                {
                    // NG
                    alert('fatal error');
                }
            }).fail(function(data, textStatus, errorThrown){
                alert('connection error');
            });
        }

        function createClient(clientName)
        {
            if(clientName == '')
            {
                alert('顧客名を入力してください。');
                return false;
            }

            // ここに追加処理(Ajax)
            ajaxCreateClient(clientName);
        }

        function ajaxCreateClient(clientName)
        {
            var data = {
                client_name: clientName
            };

            $.ajax({
                url: '/client/storeQuick',
                type: "post",
                contentType: 'application/json',
                data: JSON.stringify(data),
                dataType: 'json'
            }).done(function(data, textStatus, jqXHR){
                if(data.status == 1)
                {
                    // OK
                    console.log(data);
                    alert('顧客を登録しました。後ほど顧客情報を追記してください。');

                    // 追加顧客を選択状態へ
                    var newClientId = data.view_data.client.client_id;
                    $('.select-client-id option').attr("selected",false);
                    $('.select-client-id option:selected').attr("selected",false);
                    $('.select-client-id').append('<option value="' + newClientId + '" selected>' + clientName + '</option>');
                    $('.select-client-id').trigger('chosen:updated');

                    $('#modal').modal('hide');
                }
                else
                {
                    // NG
                    alert('fatal error');
                }
            }).fail(function(data, textStatus, errorThrown){
                alert('connection error');
            });
        }


    </script>
@endsection