@extends('template.master')
@section('contents')
<!-- Content area -->
<div class="row">
    <div class="col-lg-7">
        <div class="panel panel-success">
            <div class="panel-heading">
                <div style="position:absolute;">{{ $page['title'] }}</div>
                <div style="text-align: right"><span class="responsive-display-inline">検索： </span><input class="fm_search" type="text" placeholder="検索ワード" style="height:20px"></div>
            </div>
            <table id="datatables" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>プロジェクト名</th>
                    <th>ステータス</th>
                    <th>顧客名</th>
                    <th>担当者</th>
                    <th>利用日時</th>
                </tr>
                </thead>
                <tbody id="sort_table">
                @foreach($projects as $key => $project)
                <tr class="list_tr">
                    <td>{{ $key+1 }}</td>
                    <td><span class="tx-project_name tx_link" data-uuid="{{ $project->project_uuid }}">{{ $project->project_name }}</span></td>
                    <td class="tx-project_status">{{ $project->reservation_status_name }}</td>
                    <td class="tx-client_name">{{ $project->client_name }}</td>
                    <td class="tx-staff_name">{{ $project->account_name }}</td>
                    <td class="tx-utilization_time">{{ $project->start_time }} ~ {{ $project->termination_time }}</td>
                </tr>
                @endforeach

                </tbody>
            </table>
        </div>
        <div class="responsive-display heading-text pull-right" style="margin-top:-10px;margin-bottom:50px;padding-right:10px;"><a href="/project/create"><img class="btn-img" src="/assets/img/btn/plus.png" width="50px" height="50px"></a></div>
    </div>
</div>
<!-- /main charts -->
@endsection
@section('scripts')
<script type="text/javascript">

    (function($){

        $(document).on('click', '.tx-project_name', function(){
            var uuid = $(this).attr('data-uuid');
            location.href = '/project/show?uuid=' + uuid;
        });

        $('.fm_search').on('input', function(){
            var search_range = ['tx-project_name','tx-project_status','tx-client_name','tx-staff_name','tx-utilization_time'];
            searchWord(search_range);
        });

        $('#datatables').addClass('nowrap').dataTable({
            responsive: true,
            lengthChange: false,
            searching: false,
            ordering: false,
            info: false,
            paging: false,
            language: {
                "paginate" : {
                    "first" : "先頭",
                    "previous" : "前へ",
                    "next" : "次へ",
                    "last" : "末尾"
                }
            }
        });

    })(jQuery);

</script>
@endsection