<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-language" content="ja">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" href="http://192.168.33.10/webclip.png" />
    <title>{{ $site['name'] }}</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
    <link href="/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/core.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/r-2.2.1/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link href="/assets/css/select-multiple.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/js/lib/pickadate/themes/classic.css">
    <link rel="stylesheet" href="/assets/js/lib/pickadate/themes/classic.date.css">
    <link rel="stylesheet" href="/assets/js/lib/chosen/chosen.css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="/assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/loaders/blockui.min.js"></script>
    <script type="text/javascript" src="/assets/js/lib/select-multiple.js"></script>
    <script type="text/javascript" src="/assets/js/lib/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/r-2.2.1/datatables.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.js"></script>
    <script type="text/javascript" src="/assets/js/core/common.js"></script>
    <script type="text/javascript" src="/assets/js/lib/pickadate/picker.js"></script>
    <script type="text/javascript" src="/assets/js/lib/pickadate/picker.date.js"></script>
    <script type="text/javascript" src="/assets/js/lib/pickadate/legacy.js"></script>
    <script type="text/javascript" src="/assets/js/lib/pickadate/lang-ja.js"></script>
    <script type="text/javascript" src="/assets/js/lib/moment.js"></script>
    <script type="text/javascript" src="/assets/js/lib/chosen/chosen.jquery.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="/assets/js/plugins/visualization/d3/d3.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="/assets/js/core/app.js"></script>
</head>

<body>
<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="/" style="padding: 10px 20px;"><img src="/assets/img/logo_head.png" alt="" style="margin:0;height: 28px"></a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>

        <p class="navbar-text"><span class="label bg-success">ログイン中</span></p>

        <ul class="nav navbar-nav navbar-right">

            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="/assets/img/icons/1521421096WAdz0FGlsIVIHUmR0pCC.jpeg" alt="" width="30px" height="30px">
                    <span>{{ $myAccount->last_name }} {{ $myAccount->first_name }}</span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="/account/edit/"><i class="icon-cog5"></i> アカウント編集</a></li>
                    <li class="divider"></li>
                    <li><a href="/logout"><i class="icon-switch2"></i> ログアウト</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->
                <!--<div class="sidebar-user">-->
                <!--<div class="category-content">-->
                <!--<div class="media">-->
                <!--<a href="/profile" class="media-left"><img src="/assets/img/icons/1521421096WAdz0FGlsIVIHUmR0pCC.jpeg" class="img-circle img-sm" alt=""></a>-->
                <!--<div class="media-body">-->
                <!--<span class="media-heading text-semibold">Tsubasa Otsuka</span>-->
                <!--<div class="text-size-mini text-muted">-->

                <!--あるかぶる案件ポータル-->
                <!--</div>-->
                <!--</div>-->

                <!--<div class="media-right media-middle">-->
                <!--<ul class="icons-list">-->
                <!--<li>-->
                <!--<a href="/profile/edit"><i class="icon-cog3"></i></a>-->
                <!--</li>-->
                <!--</ul>-->
                <!--</div>-->
                <!--</div>-->
                <!--</div>-->
                <!--</div>-->
                <!-- /user menu -->


                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">

                            <!-- Main -->
                            <li class="navigation-header"><span>メインメニュー</span> <i class="icon-menu" title="Main pages"></i></li>
                            <li @if($page['dir'][0]=='dashboard')class="active"@endif><a href="/"><i class="icon-home4"></i> <span>ダッシュボード</span></a></li>
                            <li @if($page['dir'][0]=='project')class="active"@endif>
                                <a href="#"><i class="icon-calendar2"></i> <span>プロジェクト</span></a>
                                <ul>
                                    <li><a href="/project">プロジェクト一覧</a></li>
                                    <li><a href="/project/create">プロジェクト登録</a></li>
                                </ul>
                            </li>
                            <li @if($page['dir'][0]=='client')class="active"@endif>
                                <a href="#"><i class="icon-user"></i> <span>顧客</span></a>
                                <ul>
                                    <li><a href="/client">顧客一覧</a></li>
                                    <li><a href="/client/create">顧客登録</a></li>
                                </ul>
                            </li>
                            <li @if($page['dir'][0]=='invoice')class="active"@endif>
                                <a href="#"><i class="icon-balance"></i> <span>請求</span></a>
                                <ul>
                                    <li><a href="/invoice/">請求一覧</a></li>
                                    <li><a href="/invoice/create">請求登録</a></li>
                                </ul>
                            </li>
                            <li @if($page['dir'][0]=='account')class="active"@endif>
                                <a href="#"><i class="icon-user-tie"></i> <span>社員</span></a>
                                <ul>
                                    <li><a href="/account/">社員一覧</a></li>
                                    <li><a href="/account/create">社員登録</a></li>
                                </ul>
                            </li>
                            <!-- /main -->
                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->

            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-default">
                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        <li class="active"><a href="/"><i class="icon-home2 position-left"></i> ホーム</a></li>

                    </ul>
                </div>
            </div>
            <!-- /page header -->

            <div class="content">

            @yield('contents')

                    <!-- Footer -->
                    <div class="footer text-muted">
                        &copy; Copyright 2018 リーテックコーポレーション All Rights Reserved.
                    </div>
                    <!-- /footer -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->
    @yield('scripts')
</body>
</html>