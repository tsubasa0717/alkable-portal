<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-language" content="ja">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ギャラリーあるかぶる案件ポータル</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="/assets/login/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/login/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/login/css/addition.css">
    <link rel="stylesheet" href="/assets/login/css/style.css">
    <link rel="stylesheet" href="/assets/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="{{ (empty($_SERVER["HTTPS"]) ? "http://" : "https://").$_SERVER["HTTP_HOST"]."/webclip.png" }}">
    <link rel="apple-touch-icon" href="{{ (empty($_SERVER["HTTPS"]) ? "http://" : "https://").$_SERVER["HTTP_HOST"]."/webclip.png" }}" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ (empty($_SERVER["HTTPS"]) ? "http://" : "https://").$_SERVER["HTTP_HOST"]."/webclip.png" }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ (empty($_SERVER["HTTPS"]) ? "http://" : "https://").$_SERVER["HTTP_HOST"]."/webclip.png" }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ (empty($_SERVER["HTTPS"]) ? "http://" : "https://").$_SERVER["HTTP_HOST"]."/webclip.png" }}">
    <link rel="apple-touch-icon-precomposed" href="{{ (empty($_SERVER["HTTPS"]) ? "http://" : "https://").$_SERVER["HTTP_HOST"]."/webclip.png" }}">

</head>
<body>
<!-- Top content -->
<div class="top-content">

    @yield('contents')

</div>


<!-- Javascript -->
<script src="/assets/login/js/jquery-1.11.1.min.js"></script>
<script src="/assets/login/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/login/js/jquery.backstretch.min.js"></script>
<script src="/assets/login/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="/assets/login/js/placeholder.js"></script>
<![endif]-->

@yield('scripts')

</body>

</html>
