@extends('template.master')
@section('contents')
<!-- Content area -->
<div class="row">
    <div class="col-lg-7">
        <div class="deco panel panel-flat">
            <div class="panel-heading form_head">
                <h5 class="panel-title">本日の利用</h5>
                <div class="btn-use-area">
                    @foreach($todayProjects as $project)
                    <div class="inline-block">
                        <button type="button" class="btn-use btn @if($project->reservation_status == 0) btn-default @elseif($project->reservation_status == 1) btn-success @elseif($project->reservation_status == 2||$project->reservation_status == 3) btn-danger @elseif($project->reservation_status == 4||$project->reservation_status == 5||$project->reservation_status == 100) btn-primary @endif" data-uuid="{{ $project->project_uuid }}" value="0">{{ $project->start_time }}~{{ $project->termination_time }}<br>{{ $project->client_name }}様</button>
                    </div>
                    @endforeach
                    {{--<div class="inline-block">--}}
                        {{--<button type="button" class="btn-use btn btn-primary" value="0">16:00~17:00<br>小笠原 仁様</button>--}}
                    {{--</div>--}}
                    <div class="inline-block ">
                        <div>■ : 利用前</div>
                        <div>■ : 利用中</div>
                        <div>■ : 利用終了</div>
                    </div>
                </div>
            </div>
    </div>
</div>
<!-- /main charts -->

<!-- Modal windoow -->
<div id="modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>
<!-- /Modal windoow -->
@endsection
@section('scripts')
<script type="text/javascript">

    (function($){

        $(document).on('click', '.btn-use', function()
        {
            var uuid = $(this).attr('data-uuid');
            console.log(uuid);
            location.href = '/use?uuid=' + uuid;
        });

    })(jQuery);
</script>
@endsection