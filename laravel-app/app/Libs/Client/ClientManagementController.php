<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Board\BoardController;
use App\Http\Requests\updateOrCreateClientRequest;
use App\Libs\Common;
use App\Models\t_clients;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ClientManagementController extends Controller
{
    // 詳細取得
    public function getClientData(Request $request)
    {
        $view_data = [];
        $response = [
            'status' => 0,
            'message' => '',
            'view_data' => $view_data,
        ];

        if(!$request->filled('uuid'))
        {
            return response()->json($response);
        }

        $client_uuid = $request->uuid;
        $dbClient = t_clients::whereClientUuid($client_uuid)->first();

        $client = [
            'client_name' => $dbClient->client_name,
            'client_tel' => $dbClient->client_tel != '' ? $dbClient->client_tel : '登録なし',
            'client_email' => $dbClient->client_email != '' ? $dbClient->client_email : '登録なし',
            'client_comment' => $dbClient->client_comment != '' ? $dbClient->client_comment : 'コメントなし',
            'client_use_times' => $dbClient->use_times,
            'client_summary_time' => $dbClient->summary_time,
            'client_projects' => $dbClient->client_projects,
        ];

        foreach($client['client_projects'] as $key => $project)
        {
            $client['client_projects'][$key]['reservation_status_name'] = $project->reservation_status_name;
        }

        $view_data = [
            'client' => $client,
        ];

        $response['status'] = 1;
        $response['message'] = '';
        $response['view_data'] = $view_data;

        return response()->json($response);
    }

    // 顧客更新処理
    public function update(updateOrCreateClientRequest $request)
    {
        if(!isset($request->client_uuid))
        {
            return false;
        }

        $client = t_clients::whereClientUuid($request->client_uuid)->first();

        // Board登録
        $boardClient = BoardController::updateClient($client->board_client_code,[
            'name' => $request->client_name,
            'name_disp' => $request->client_name,
            'tel' => $request->client_tel,
            'to' => $request->client_email,
        ]);

        if(!isset($boardClient['id']))
        {
            return false;
        }

        // DB登録
        $dbClient = $client->update([
            'board_client_code' => $boardClient['id'],
            'client_uuid' => Common::uuid(),
            'client_name' => $boardClient['name'],
            'client_name_disp' => $boardClient['name_disp'],
            'client_email' => $boardClient['to'],
            'client_tel' => $boardClient['tel'],
            'client_comment' => htmlspecialchars($request->client_comment, ENT_QUOTES),
            'is_black' => $request->is_black,
        ]);

        $redirectPath = '/client/edit?uuid='.$request->client_uuid;
        Session::flash('client-name', $boardClient['name']);
        Session::flash('flash_message_success', true);
        return redirect($redirectPath);

    }
}
