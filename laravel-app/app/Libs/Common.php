<?php
namespace App\Libs;

use App\Models\t_accounts;
use App\Models\t_clients;
use Carbon\Carbon;
use Yasumi\Yasumi;

class Common
{
    static $BIRTHDAY_SELECT_YEAR_MIN = 1897;

    public static function uuid()
    {
        $uuid = uniqid(mt_rand(), true);
        $uuid = sha1($uuid);
        return $uuid;
    }

    public static function formatDate($day,$hour = 0,$minute = 0,$second = 0)
    {
        $day = str_replace('年', '-', $day);
        $day = str_replace('月', '-', $day);
        $day = str_replace('日', '', $day);

        if($hour+$minute+$second == 0)
        {
            $format = $day;
        }
        else{
            $format = $day.' '.$hour.':'.$minute.':'.$second;
        }

        return $format;
    }

    public static function changeClientId2Code($id)
    {
        $client = t_clients::find($id);
        return $client->board_client_code;
    }

    public static function changeAccountId2Code($id)
    {
        $account = t_accounts::find($id);
        return $account->board_account_code;
    }

    public static function changeInvoiceType2Name($num)
    {
        if($num<0||$num>3)
        {
            return false;
        }

        $invoice_name = '';
        if($num == 0)
        {
            $invoice_name = '利用料金';
        }
        else if($num == 1)
        {
            $invoice_name = '利用延長料';
        }
        else if($num == 2)
        {
            $invoice_name = '違約金';
        }
        else if($num == 3)
        {
            $invoice_name = 'その他';
        }

        return $invoice_name;
    }

    public static function isHoliday($day)
    {
        $formatDay = self::formatDate($day);
        $year = Carbon::parse($formatDay)->year;
        $result = [
            'status' => false,
            'detail' => '平日',
        ];

        // 祝日判定
        $holidays = Yasumi::create('Japan',$year,'ja_JP');
        if($holidays->isHoliday(new \DateTime($formatDay)))
        {
            $name = '祝日';
            foreach ($holidays as $holiday)
            {
                if($holiday->format('Y-m-d') == $formatDay)
                {
                    $name = $holiday->getName();
                }
            }

            $result = [
                'status' => true,
                'detail' => $name,
            ];

            return $result;
        }

        // 曜日判定
        $day = Carbon::parse($formatDay);
        if($day->isWeekend())
        {
            $result = [
                'status' => true,
                'detail' => '週末',
            ];

            return $result;
        }

        return $result;
    }
}