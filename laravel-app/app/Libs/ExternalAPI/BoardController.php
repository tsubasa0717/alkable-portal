<?php

namespace App\Http\Controllers\Board;

use App\Models\t_accounts;
use App\Models\t_clients;
use App\Http\Controllers\Controller;
use App\Models\t_invoices;
use App\Models\t_projects;
use Illuminate\Support\Facades\DB;

class BoardController extends Controller
{
    // boardAPI通信
    static function runApi($api_data)
    {
        $header = [
            'Authorization: Bearer '.env('BOARD_API_ACCESS_TOKEN'),  // 前準備で取得したtokenをヘッダに含める
            'x-api-key: '.env('BOARD_API_KEY'),
            'Content-Type: application/json',
        ];

        //curl実行
        $ch = curl_init();
        if($api_data['api_method']=='GET')
        {
            curl_setopt($ch, CURLOPT_URL, env('BOARD_END_POINT').$api_data['api_URL']."?".http_build_query($api_data['post_data']));
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 証明書の検証を行わない
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  // curl_execの結果を文字列で返す
        }
        else
        {
            curl_setopt($ch, CURLOPT_URL, env('BOARD_END_POINT').$api_data['api_URL']);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $api_data['api_method']);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($api_data['post_data']));
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        }

        $response = curl_exec($ch);
        $result = json_decode($response, true);
        curl_close($ch);

        return $result;
    }

    // 顧客登録
    static function createClient($data)
    {
        $api_data = [
            'api_method' => 'POST',
            'api_URL' => '/v1/clients',
            'post_data' => $data
        ];

        $result = self::runApi($api_data);

        return $result;
    }

    // 全顧客取得
    static function showClients()
    {
        $api_data = [
            'api_method' => 'GET',
            'api_URL' => '/v1/clients/',
            'post_data' => [
                'response_group' => 'large',
            ]
        ];

        $result = self::runApi($api_data);

        return $result;
    }

    // 顧客取得
    static function showClient($code)
    {
        $api_data = [
            'api_method' => 'GET',
            'api_URL' => '/v1/clients/'.$code,
            'post_data' => []
        ];

        $result = self::runApi($api_data);

        return $result;
    }

    // 顧客更新
    static function updateClient($code,$data)
    {
        $api_data = [
            'api_method' => 'PATCH',
            'api_URL' => '/v1/clients/'.$code,
            'post_data' => $data
        ];

        $result = self::runApi($api_data);

        return $result;
    }

    // 顧客削除
    static function deleteClient($code)
    {
        $api_data = [
            'api_method' => 'DELETE',
            'api_URL' => '/v1/clients/'.$code,
            'post_data' => []
        ];

        $result = self::runApi($api_data);

        return $result;
    }

    // プロジェクト区分取得
    static function showProjectTypes()
    {
        $api_data = [
            'api_method' => 'GET',
            'api_URL' => '/v1/project_types/',
            'post_data' => [
                'project_type_kbn_eq' => '2'
            ]
        ];

        $result = self::runApi($api_data);

        return $result;
    }

    // プロジェクト登録
    static function createProject($data)
    {
        $api_data = [
            'api_method' => 'POST',
            'api_URL' => '/v1/projects',
            'post_data' => $data
        ];

        $result = self::runApi($api_data);

        return $result;
    }

    // 全プロジェクト取得
    static function showProjects()
    {
        $api_data = [
            'api_method' => 'GET',
            'api_URL' => '/v1/projects/',
            'post_data' => [
                'response_group' => 'all'
            ]
        ];

        $result = self::runApi($api_data);

        return $result;
    }

    // プロジェクト取得
    static function showProject($code)
    {
        $api_data = [
            'api_method' => 'GET',
            'api_URL' => '/v1/projects/'.$code,
            'post_data' => []
        ];

        $result = self::runApi($api_data);

        return $result;
    }

    // プロジェクト更新
    static function updateProject($code,$data)
    {
        $api_data = [
            'api_method' => 'PATCH',
            'api_URL' => '/v1/projects/'.$code,
            'post_data' => $data
        ];

        $result = self::runApi($api_data);

        return $result;
    }

    // プロジェクト削除
    static function deleteProject($code)
    {
        $api_data = [
            'api_method' => 'DELETE',
            'api_URL' => '/v1/projects/'.$code,
            'post_data' => []
        ];

        $result = self::runApi($api_data);

        return $result;
    }

    // 請求書リスト取得
    static function showInvoices()
    {
        $api_data = [
            'api_method' => 'GET',
            'api_URL' => '/v1/invoices',
            'post_data' => [
                'response_group' => 'invoice'
            ]
        ];

        $result = self::runApi($api_data);

        return $result;
    }

    // 請求書取得
    static function showInvoice($code)
    {
        $api_data = [
            'api_method' => 'GET',
            'api_URL' => '/v1/documents/invoices/'.$code,
            'post_data' => []
        ];

        $result = self::runApi($api_data);

        return $result;
    }

    // 請求書更新
    static function updateInvoice($code,$data)
    {
        $api_data = [
            'api_method' => 'PATCH',
            'api_URL' => '/v1/documents/invoices/'.$code,
            'post_data' => $data
        ];

        $result = self::runApi($api_data);

        return $result;
    }

    // 請求ステータス更新
    static function updateInvoiceStatus($code,$data)
    {
        $api_data = [
            'api_method' => 'PATCH',
            'api_URL' => '/v1/invoices/invoice_status/'.$code,
            'post_data' => $data
        ];

        $result = self::runApi($api_data);

        return $result;
    }

    // 見積書取得
    static function showEstimate($code)
    {
        $api_data = [
            'api_method' => 'GET',
            'api_URL' => '/v1/documents/estimates/'.$code,
            'post_data' => []
        ];

        $result = self::runApi($api_data);

        return $result;
    }

    // 見積書更新
    static function updateEstimate($code,$data)
    {
        $api_data = [
            'api_method' => 'PATCH',
            'api_URL' => '/v1/documents/estimates/'.$code,
            'post_data' => $data
        ];

        $result = self::runApi($api_data);

        return $result;
    }

    // 見積書取得
    static function showReceipt($code)
    {
        $api_data = [
            'api_method' => 'GET',
            'api_URL' => '/v1/documents/receipts/'.$code,
            'post_data' => []
        ];

        $result = self::runApi($api_data);

        return $result;
    }

    // 見積書更新
    static function updateReceipt($code,$data)
    {
        $api_data = [
            'api_method' => 'PATCH',
            'api_URL' => '/v1/documents/receipts/'.$code,
            'post_data' => $data
        ];

        $result = self::runApi($api_data);

        return $result;
    }

    // 社員取得
    static function showUsers()
    {
        $api_data = [
            'api_method' => 'GET',
            'api_URL' => '/v1/users',
            'post_data' => []
        ];

        $result = self::runApi($api_data);

        return $result;
    }

    // 顧客DB反映
    static function optimizationClients()
    {
        DB::beginTransaction();
        try
        {
            $clients = BoardController::showClients();

            foreach ($clients as $client)
            {
                // DBデータ
                $client_data = [
                    'board_client_code' => $client['id'],
                    'client_name' => $client['name'],
                    'client_name_disp' => $client['name_disp'],
                    'client_tel' => $client['tel'],
                    'client_email' => $client['to'],
                    'client_comment' => $client['note'],
                ];

                // ポータル内DB操作
                t_clients::updateOrCreate([
                    'board_client_code'=> $client['id']
                ],$client_data);
            }

            DB::commit();
            return true;
        }
        catch (\PDOException $e)
        {
            DB::rollBack();
            return false;
        }
    }

    // 請求DB反映
    static function optimizationInvoices()
    {
        DB::beginTransaction();
        try
        {
            $invoices = BoardController::showProjects();

            foreach ($invoices as $invoice)
            {
                // DBデータ
                $invoice_data = [
                    'board_project_code' => $invoice['id'],
                    'project_id' => $invoice['tags'][1],
                    'board_invoice_code' => $invoice['invoices'][0]['id'],
                    'board_estimate_code' => $invoice['invoices'][0]['id'],
                    'board_receipt_code' => $invoice['invoices'][0]['id'],
                    'invoice_name' => $invoice['name'],
                    'invoice_type' => 0,
                    'invoice_amount' => ($invoice['invoice_total']),
                    'invoice_remain_amount' => ($invoice['invoice_total']),
                    'invoice_status' => 0,
                    'invoice_deadline' => $invoice['invoice_dates'][0],
                ];

                // ポータル内DB操作
                t_invoices::updateOrCreate([
                    'board_project_code'=> $invoice['id']
                ],$invoice_data);
            }

            DB::commit();
            return true;
        }
        catch (\PDOException $e)
        {
            DB::rollBack();
            return false;
        }
    }

    // 社員DB反映
    static function optimizationAccounts()
    {
        DB::beginTransaction();
        try
        {
            $accounts = BoardController::showUsers();

            foreach ($accounts as $account)
            {
                // DBデータ
                $account_data = [
                    'board_account_code' => $account['id'],
                    'last_name' => $account['last_name'],
                    'first_name' => $account['first_name'],
                    'email' => $account['email'],
                ];

                // ポータル内DB操作
                t_accounts::updateOrCreate([
                    'board_account_code'=> $account['id']
                ],$account_data);
            }

            DB::commit();
            return true;
        }
        catch (\PDOException $e)
        {
            DB::rollBack();
            return false;
        }
    }

    // プロジェクトDB反映
    static function optimizationProjects()
    {
        DB::beginTransaction();
        try
        {
            $projects = BoardController::showProjects();

            foreach ($projects as $project)
            {
                $client = t_clients::whereBoardClientCode($project['client']['id']);
                $account = t_accounts::whereBoardAccountCode($project['user']['id']);

                if(!$client->exists() || !$account->exists())
                {
                    return false;
                }

                // DBデータ
                $project_data = [
                    'project_id' => $project['tags'][1],
                    'project_name' => $project['name'],
                    'google_calendar_code' => '',
                    'client_id' => $client->first()->client_id,
                    'account_id' => $account->first()->account_id,
                    'reservation_status' => 0,
                ];

                // ポータル内DB操作
                t_projects::updateOrCreate([
                    'project_id'=> $project['tags'][1]
                ],$project_data);
            }

            DB::commit();
            return true;
        }
        catch (\PDOException $e)
        {
            DB::rollBack();
            dump($e);
            return false;
        }
    }

}
