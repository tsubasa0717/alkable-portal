<?php

namespace App\Http\Controllers\ExternalAPI;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use phpDocumentor\Reflection\Types\Self_;

class GoogleCalendarController extends Controller
{
    //
    public static function createService($target)
    {
        if($target == 'gallery')
        {
            $colorId = 1;
        }
        elseif($target == 'account')
        {
            $colorId = 2;
        }
        else
        {
            return false;
        }

        $calendarId = env('Gallery_GOOGLE_CALENDAR_ID');
        $key_path = storage_path('app/json/google_credential.json');
        putenv('GOOGLE_APPLICATION_CREDENTIALS='. $key_path);
        $client = new \Google_Client();
        $client->useApplicationDefaultCredentials();

        $client->addScope(\Google_Service_Calendar::CALENDAR);
        $service = new \Google_Service_Calendar($client);

        return [$service,$calendarId,$colorId];
    }

    public static function insert($target,$data)
    {
        list($service,$calendarId,$colorId) = GoogleCalendarController::createService($target);

        // データ整形
        $schedule = [
            'summary' => $data['summary'],
            'location' => $data['location'],
            'description' => $data['description'],
            'colorId' => $colorId,
            'start' => [
                'dateTime' => Carbon::parse($data['start'])->toIso8601String(),
                'timeZone' => 'Asia/Tokyo',
            ],
            'end' => [
                'dateTime' => Carbon::parse($data['end'])->toIso8601String(),
                'timeZone' => 'Asia/Tokyo',
            ]
        ];

        $event = new \Google_Service_Calendar_Event($schedule);
        $new_event = $service->events->insert($calendarId, $event);

        return $new_event;
    }

    public static function update($target,$eventId,$data)
    {
        list($service,$calendarId,$colorId) = GoogleCalendarController::createService($target);

        // データ整形
        $schedule = [
            'summary' => $data['summary'],
            'location' => $data['location'],
            'description' => $data['description'],
            'colorId' => $colorId,
            'start' => [
                'dateTime' => Carbon::parse($data['start'])->toIso8601String(),
                'timeZone' => 'Asia/Tokyo',
            ],
            'end' => [
                'dateTime' => Carbon::parse($data['end'])->toIso8601String(),
                'timeZone' => 'Asia/Tokyo',
            ]
        ];

        $event = new \Google_Service_Calendar_Event($schedule);
        $new_event = $service->events->update($calendarId, $eventId,$event);

        return $new_event;
    }

    public static function delete($target,$eventId)
    {
        list($service,$calendarId,$colorId) = GoogleCalendarController::createService($target);
        $new_event = $service->events->delete($calendarId, $eventId);

        return $new_event;
    }

    public static function getEvent($target,$eventId)
    {

        list($service,$calendarId,$colorId) = GoogleCalendarController::createService($target);
        $result = $service->events->get($calendarId, $eventId);

        return $result;
    }

    public static function getEventList($target,$options)
    {

        list($service,$calendarId,$colorId) = GoogleCalendarController::createService($target);

        // オプション整形
        $newOptions = array(
            'maxResults' => 10,
            'orderBy' => 'startTime',
            'singleEvents' => TRUE,
            'timeMax' => Carbon::parse($options['to'])->toIso8601String(),
            'timeMin' => Carbon::parse($options['from'])->toIso8601String(),
        );

        $result = $service->events->listEvents($calendarId, $newOptions)->items;

        return $result;
    }

    public static function checkEmpty($from,$to)
    {
        $option = [
            'from' => $from,
            'to' => $to
        ];

        // 検索結果があればNG
        $events = self::getEventList('gallery',$option);
        if(count($events)!=0)
        {
            return [false,$events];
        }

        return [true,[]];
    }
}
