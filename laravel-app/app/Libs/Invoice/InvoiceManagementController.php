<?php

namespace App\Http\Controllers\Invoice;

use App\Http\Controllers\Board\BoardController;
use App\Http\Requests\updateOrCreateInvoiceRequest;
use App\Libs\Common;
use App\Models\t_clients;
use App\Models\t_invoices;
use App\Models\t_payments;
use App\Models\t_projects;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class InvoiceManagementController extends Controller
{
    // 詳細取得
    public function getInvoiceData(Request $request)
    {
        $view_data = [];
        $response = [
            'status' => 0,
            'message' => '',
            'view_data' => $view_data,
        ];

        if(!$request->filled('uuid'))
        {
            return response()->json($response);
        }

        $invoice = t_invoices::whereInvoiceUuid($request->uuid)->first();
        $project = t_projects::whereProjectId($invoice->project_id)->first();
        $client = t_clients::whereClientId($project->client_id)->first();

        $invoice->invoice_view_status_name = $invoice->invoice_status_name;
        $invoice->invoice_deadline = Carbon::parse($invoice->invoice_deadline)->format('Y/m/d');
        $invoice->invoice_project_name = $project->project_name;
        $invoice->invoice_client_name = $client->client_name;

        $payments = t_payments::whereInvoiceId($invoice->invoice_id)->orderBy('created_at','asc')->get();

        $view_data = [
            'invoice' => $invoice,
            'payments' => $payments,
        ];

        $response['status'] = 1;
        $response['message'] = '';
        $response['view_data'] = $view_data;

        return response()->json($response);
    }

    //  請求更新処理
    public function update(updateOrCreateInvoiceRequest $request)
    {
        if(!isset($request->invoice_uuid))
        {
            return false;
        }

        $invoice = t_invoices::whereInvoiceUuid($request->invoice_uuid)->first();
        $project = t_projects::whereProjectId($request->project_id)->first();

        // Boardプロジェクト登録
        $boardProject = BoardController::updateProject($invoice->board_project_code,[
            "name" => Common::changeInvoiceType2Name($request->invoice_type),
            "client_id"=> Common::changeClientId2Code($project->client_id),
            "user_id" => Common::changeAccountId2Code($project->account_id),
            "tags" => ["あるかぶる案件","$project->project_id"]
        ]);

        // Board見積り&請求登録
        $document_data = [
            "total" => $request->invoice_amount,
            "tax" => $request->invoice_amount * env('ConsumptionTaxFee')/100,
            "message" => Common::changeInvoiceType2Name($request->invoice_type),
            "details" => [
                [
                    "description" => Common::changeInvoiceType2Name($request->invoice_type),
                    "quantity" => 1,
                    "unit" => "式",
                    "unit_price" => $request->invoice_type
                ],
            ]
        ];
        $boardInvoice = BoardController::updateInvoice($invoice->board_invoice_code,$document_data);
        $boardEstimate = BoardController::updateEstimate($invoice->board_estimate_code,$document_data);

        // DB請求登録
        $dbInvoice = $invoice->update([
            'project_id' => $project->project_id,
            'invoice_name' => Common::changeInvoiceType2Name($request->invoice_type),
            'invoice_type' => $request->invoice_type,
            'invoice_amount' => $request->invoice_amount,
            'invoice_remain_amount' => $request->invoice_amount,
            'invoice_deadline' => Common::formatDate($request->invoice_deadline,23,59,59),
        ]);

        $redirectPath = '/invoice/edit?uuid='.$request->invoice_uuid;
        Session::flash('invoice-name', Common::changeInvoiceType2Name($request->invoice_type));
        Session::flash('flash_message_success', true);
        return redirect($redirectPath);
    }
}
