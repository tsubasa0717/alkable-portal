<?php

namespace App\Http\Controllers\Production;

use App\Http\Controllers\Board\BoardController;
use App\Http\Controllers\ExternalAPI\GoogleCalendarController;
use App\Http\Controllers\Project\ProjectController;
use App\Http\Controllers\Project\ProjectManagementController;
use App\Libs\Common;
use App\Models\t_invoices;
use App\Models\t_projects;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductionManagementController extends Controller
{
    // 利用開始処理
    public function startUse(Request $request)
    {
        $view_data = [];
        $response = [
            'status' => 0,
            'message' => '',
            'view_data' => $view_data,
        ];

        if(!$request->filled('uuid'))
        {
            return response()->json($response);
        }

        if(ProjectManagementController::startUse($request->uuid))
        {
            $response['status'] = 1;
        }

        $view_data = [];
        $response['message'] = '';
        $response['view_data'] = $view_data;

        return response()->json($response);
    }

    // 延長可否チェック
    public function checkExtension(Request $request)
    {
        $view_data = [];
        $response = [
            'status' => 0,
            'message' => '',
            'view_data' => $view_data,
        ];

        if(!$request->filled('uuid'))
        {
            return response()->json($response);
        }

        $project = t_projects::whereProjectUuid($request->uuid)->first();

        // 営業時間内チェック
        if(Carbon::parse($project->termination_time)->addHour($request->extension_time)->hour > 20)
        {
            $response['status'] = -1;
            $response['message'] = "延長出来ませんでした。\n理由：営業時間外。";
        }
        else
        {
            // 空き状況チェック
            $checkEmpty = GoogleCalendarController::checkEmpty($project->termination_time,Carbon::parse($project->termination_time)->addHour($request->extension_time + 1)->format('Y-m-d H:i:s'));
            if($checkEmpty[0])
            {
                $response['status'] = 1;
            }
            else
            {
                // TODO: 社員スケジュールでのチェック
                $response['status'] = -1;
                $response['message'] = "延長出来ませんでした。\n理由：他の利用時間と重複。";
            }
        }


        $view_data = [];
        $response['view_data'] = $view_data;

        return response()->json($response);
    }

    // 延長処理
    public function doExtension(Request $request)
    {
        $view_data = [];
        $response = [
            'status' => 0,
            'message' => '',
            'view_data' => $view_data,
        ];

        if(!$request->filled('uuid'))
        {
            return response()->json($response);
        }

        $project = t_projects::whereProjectUuid($request->uuid)->first();

        // プロジェクトDB更新
        $project->update([
            'termination_time' => Carbon::parse($project->termination_time)->addHour($request->extension_time)
        ]);

        // 請求登録
        // Boardプロジェクト登録
        $boardProject = BoardController::createProject([
            "name" => Common::changeInvoiceType2Name(1),
            "client_id"=> Common::changeClientId2Code($project->client_id),
            "user_id" => Common::changeAccountId2Code($project->account_id),
            "estimate_date" => Carbon::today()->toDateString(),
            "order_status" => 1,
            "invoice_timing_kbn"=> 1,
            "invoice_date"=> Carbon::today()->toDateString(),
            "tags" => ["あるかぶる案件","$project->project_id"]
        ]);

        // Board見積り&請求登録
        $document_data = [
            "total" => $request->fee,
            "tax" => $request->fee * env('ConsumptionTaxFee')/100,
            "message" => Common::changeInvoiceType2Name(1),
            "details" => [
                [
                    "description" => Common::changeInvoiceType2Name(1),
                    "quantity" => 1,
                    "unit" => "式",
                    "unit_price" => 2
                ],
            ]
        ];
        $boardInvoice = BoardController::updateInvoice($boardProject['invoices'][0]['id'],$document_data);
        $boardEstimate = BoardController::updateEstimate($boardProject['estimate']['id'],$document_data);
        BoardController::updateInvoiceStatus($boardProject['invoices'][0]['id'],[
            'invoice_status' => 3,
        ]);

        // DB請求登録
        $dbInvoice = t_invoices::create([
            'board_project_code' => $boardProject['id'],
            'project_id' => $project->project_id,
            'board_invoice_code' => $boardProject['invoices'][0]['id'],
            'board_estimate_code' => $boardProject['estimate']['id'],
            'board_receipt_code' => $boardProject['receipts'][0]['id'],
            'invoice_name' => Common::changeInvoiceType2Name(1),
            'invoice_type' => 1,
            'invoice_amount' => $request->fee,
            'invoice_remain_amount' => 0,
            'invoice_status' => 2,
            'invoice_deadline' => Common::formatDate(Carbon::parse('1 month')->endOfMonth()->format('Y-m-d'),23,59,59),
        ]);

        $response['status'] = 1;
        $view_data = [];
        $response['view_data'] = $view_data;

        return response()->json($response);
    }

    // 利用終了待機処理
    public function endStandbyUse(Request $request)
    {
        $view_data = [];
        $response = [
            'status' => 0,
            'message' => '',
            'view_data' => $view_data,
        ];

        if(!$request->filled('uuid'))
        {
            return response()->json($response);
        }

        if(ProjectManagementController::changeReservationStatus($request->uuid))
        {
            $response['status'] = 1;
        }

        $view_data = [];
        $response['message'] = '';
        $response['view_data'] = $view_data;

        return response()->json($response);
    }

    // 利用終了処理
    public function endUse(Request $request)
    {
        $view_data = [];
        $response = [
            'status' => 0,
            'message' => '',
            'view_data' => $view_data,
        ];

        if(!$request->filled('uuid'))
        {
            return response()->json($response);
        }

        if(ProjectManagementController::endUse($request->uuid,$request->is_penalty))
        {
            $response['status'] = 1;
        }

        $view_data = [];
        $response['message'] = '';
        $response['view_data'] = $view_data;

        return response()->json($response);
    }
}
