<?php

namespace App\Http\Controllers\Project;

use App\Http\Controllers\Board\BoardController;
use App\Http\Controllers\ExternalAPI\GoogleCalendarController;
use App\Http\Requests\updateOrCreateProjectRequest;
use App\Libs\Common;
use App\Models\t_invoices;
use App\Models\t_projects;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ProjectManagementController extends Controller
{
    // 休日判定
    public function checkHoliday(Request $request)
    {
        $view_data = [];
        $response = [
            'status' => 0,
            'message' => '',
            'view_data' => $view_data,
        ];

        if(!$request->filled('day'))
        {
            return response()->json($response);
        }

        $day = $request->day;
        $result = Common::isHoliday($day);

        $view_data = [
            'status' => $result['status'],
            'detail' => $result['detail'],
        ];

        $response['status'] = 1;
        $response['message'] = '';
        $response['view_data'] = $view_data;

        return response()->json($response);
    }

    // プロジェクト更新処理
    public function update(updateOrCreateProjectRequest $request)
    {
        if(!isset($request->project_uuid))
        {
            return false;
        }

        $project = t_projects::whereProjectUuid($request->project_uuid)->first();
        $invoice = t_invoices::whereProjectId($project->project_id)
            ->whereInvoiceType(0)
            ->first();

        // DBプロジェクト更新
        $project->update([
            'project_name' => $request->project_name,
            'project_comment' => $request->project_comment,
            'client_id' => $request->client_id,
            'account_id' => $request->account_id,
            'start_time' => Common::formatDate($request->use_day,$request->start_hour),
            'termination_time' => Common::formatDate($request->use_day,$request->end_hour),
        ]);

        // Boardプロジェクト更新
        $boardProject = BoardController::updateProject($invoice->board_project_code,[
            "name" => $request->project_name,
            "client_id"=> Common::changeClientId2Code($request->client_id),
            "user_id" => Common::changeAccountId2Code($request->account_id),
            "estimate_date" => Carbon::today()->toDateString(),
            "invoice_date"=> Carbon::today()->toDateString(),
            "tags" => ["あるかぶる案件","$project->project_id"]
        ]);

        // Google Calendar更新
        $googleCalendarEvent = GoogleCalendarController::update('gallery',$project->google_calendar_code,[
            'summary' => $request->project_name,
            'location' => '東京都目黒区上目黒２丁目４１−９',
            'description' => $request->project_comment,
            'start' => $project->start_time,
            'end' => $project->termination_time,
        ]);

        // Board見積り&請求登録
        $document_data = [
            "total" => $request->project_amount,
            "tax" => $request->project_amount * env('ConsumptionTaxFee')/100,
            "message" => "利用料金",
            "details" => [
                [
                    "description" => "利用料金",
                    "quantity" => 1,
                    "unit" => "式",
                    "unit_price" => $request->project_amount
                ],
            ]
        ];
        $boardInvoice = BoardController::updateInvoice($invoice->board_invoice_code,$document_data);
        $boardEstimate = BoardController::updateEstimate($invoice->board_estimate_code,$document_data);

        // DB請求登録
        $dbInvoice = $invoice->update([
            'invoice_amount' => $request->project_amount,
            'invoice_deadline' => Common::formatDate($request->invoice_deadline,23,59,59),
        ]);


        $redirectPath = '/project/edit?uuid='.$request->project_uuid;
        Session::flash('project-name', $boardProject['name']);
        Session::flash('flash_message_success', true);
        return redirect($redirectPath);
    }

    // 予約状態変更
    public static function changeReservationStatus($project_uuid)
    {
        $project = t_projects::whereProjectUuid($project_uuid)->first();

        // 仮予約から本予約へ変更
        if($project->reservation_status == 0)
        {
            $invoice = t_invoices::whereProjectId($project->project_id)
                ->whereInvoiceType(0)
                ->first();
            if($invoice->invoice_remain_amount == 0 && $invoice->invoice_status == 2)
            {
                $project->update([
                    'reservation_status' => 1,
                ]);
            }
        }
        // 利用中から利用終了待機へ変更
        elseif($project->reservation_status == 2)
        {
            $project->update([
                'reservation_status' => 3,
            ]);
        }

        return true;
    }

    // 利用開始
    public static function startUse($project_uuid)
    {
        $project = t_projects::whereProjectUuid($project_uuid)->first();

        // 本予約から利用開始
        if($project->reservation_status == 1)
        {
            $project->update([
                'reservation_status' => 2,
            ]);
        }

        return true;
    }

    // 利用終了
    public static function endUse($project_uuid,$is_penalty)
    {
        $project = t_projects::whereProjectUuid($project_uuid)->first();

        // 利用終了待機から利用終了
        if($project->reservation_status == 3)
        {
            // 違約金なし
            if($is_penalty == 0)
            {
                $project->update([
                    'reservation_status' => 4,
                ]);
            }
            // 違約金あり
            else
            {
                $project->update([
                    'reservation_status' => 100,
                ]);

                // 請求登録
                // Boardプロジェクト登録
                $boardProject = BoardController::createProject([
                    "name" => Common::changeInvoiceType2Name(2),
                    "client_id"=> Common::changeClientId2Code($project->client_id),
                    "user_id" => Common::changeAccountId2Code($project->account_id),
                    "estimate_date" => Carbon::today()->toDateString(),
                    "order_status" => 1,
                    "invoice_timing_kbn"=> 1,
                    "invoice_date"=> Carbon::today()->toDateString(),
                    "tags" => ["あるかぶる案件","$project->project_id"]
                ]);

                // Board見積り&請求登録
                $document_data = [
                    "total" => 0,
                    "tax" => 0 * env('ConsumptionTaxFee')/100,
                    "message" => Common::changeInvoiceType2Name(2),
                    "details" => [
                        [
                            "description" => Common::changeInvoiceType2Name(2),
                            "quantity" => 1,
                            "unit" => "式",
                            "unit_price" => 2
                        ],
                    ]
                ];
                $boardInvoice = BoardController::updateInvoice($boardProject['invoices'][0]['id'],$document_data);
                $boardEstimate = BoardController::updateEstimate($boardProject['estimate']['id'],$document_data);

                // DB請求登録
                $dbInvoice = t_invoices::create([
                    'board_project_code' => $boardProject['id'],
                    'project_id' => $project->project_id,
                    'board_invoice_code' => $boardProject['invoices'][0]['id'],
                    'board_estimate_code' => $boardProject['estimate']['id'],
                    'board_receipt_code' => $boardProject['receipts'][0]['id'],
                    'invoice_name' => Common::changeInvoiceType2Name(2),
                    'invoice_type' => 2,
                    'invoice_amount' => 0,
                    'invoice_remain_amount' => 0,
                    'invoice_status' => 0,
                    'invoice_deadline' => Common::formatDate(Carbon::parse('1 month')->endOfMonth()->format('Y-m-d'),23,59,59),
                ]);
            }
        }
        else
        {
            return false;
        }

        return true;
    }
}
