<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Board\BoardController;
use App\Http\Requests\updateAccountRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AccountManagementController extends Controller
{
    // 社員登録処理
    public function store(Request $request)
    {
        BoardController::optimizationAccounts();

        $redirectPath = '/account/create';
        Session::flash('flash_message_success', true);
        return redirect($redirectPath);
    }

    // 社員更新処理
    public function update(updateAccountRequest $request)
    {
        $account = Auth::user();
        $account->update([
            'password' => Hash::make($request->password)
        ]);

        $redirectPath = '/account/edit';
        Session::flash('flash_message_success', true);
        return redirect($redirectPath);
    }
}
