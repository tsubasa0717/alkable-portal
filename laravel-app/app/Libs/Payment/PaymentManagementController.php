<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Board\BoardController;
use App\Http\Controllers\Project\ProjectManagementController;
use App\Models\t_invoices;
use App\Models\t_payments;
use App\Models\t_projects;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentManagementController extends Controller
{
    //  入金処理
    public function payment(Request $request)
    {
        $view_data = [];
        $response = [
            'status' => 0,
            'message' => '',
            'view_data' => $view_data,
        ];

        if(!$request->filled('uuid'))
        {
            return response()->json($response);
        }

        $invoice = t_invoices::whereInvoiceUuid($request->uuid)->first();

        // 不整合な入力を弾く
        if($request->payment_amount <= 0 || !is_int($request->payment_amount))
        {
            $response['status'] = -1;
            $response['message'] = '正しい金額を入力してください。';
            return response()->json($response);
        }

        // 過払いは弾く
        if($invoice->invoice_remain_amount < $request->payment_amount)
        {
            $response['status'] = -1;
            $response['message'] = '過払いが発生しています。';
            return response()->json($response);
        }

        // DB入金データ
        $dbPayment = t_payments::create([
            'invoice_id' => $invoice->invoice_id,
            'payment_amount' => $request->payment_amount,
        ]);

        // DB請求データ
        $dbInvoice = $invoice->update([
            'invoice_status' => 1,
            'invoice_remain_amount' => $invoice->invoice_remain_amount - $request->payment_amount,
        ]);

        // 支払い終了処理
        if($invoice->invoice_remain_amount == 0)
        {
            // DB請求データ
            $dbInvoice = $invoice->update([
                'invoice_status' => 2,
            ]);

            // Board入金
            $result = BoardController::updateInvoiceStatus($invoice->board_project_code,[
                'invoice_status' => 3,
            ]);

            // 予約状態変更
            $project = t_projects::whereProjectId($invoice->project_id)->first();
            ProjectManagementController::changeReservationStatus($project->project_uuid);
        }

        $view_data = [];
        $response['status'] = 1;
        $response['message'] = '';
        $response['view_data'] = $view_data;

        return response()->json($response);
    }
}