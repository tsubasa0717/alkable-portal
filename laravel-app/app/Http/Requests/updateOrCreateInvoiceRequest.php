<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class updateOrCreateInvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_id' => 'required',
            'invoice_type' => 'required',
            'invoice_amount' => 'required|integer|min:0',
            'invoice_deadline' => 'required|date_format:"Y年m月d日"',
        ];
    }

    public function messages()
    {
        return [
            'project_id.required' => 'プロジェクトを選択してください。',
            'invoice_type.required' => '請求タイプを選択してください。',
            'invoice_amount.required' => '請求金額を設定してください。',
            'invoice_amount.integer' => '請求金額は整数で入力してください。',
            'invoice_amount.min' => '請求金額は:min円以上で入力してください。',
            'invoice_deadline.required' => '料金受付締切日を選択してください。',
            'invoice_deadline.date_format' => '料金受付締切日が不正です。',
        ];
    }
}
