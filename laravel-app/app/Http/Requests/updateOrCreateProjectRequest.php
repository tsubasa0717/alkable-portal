<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class updateOrCreateProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_name' => 'required|max:191',
            'project_comment' => 'max:191',
            'client_id' => 'required',
            'account_id' => 'required',
            'use_day' => 'required|date_format:"Y年m月d日"',
            'start_hour' => 'required|integer|between:10,19',
            'end_hour' => 'required|integer|between:11,20|gt:start_hour',
            'project_amount' => 'required|integer|min:0',
            'invoice_deadline' => 'required|date_format:"Y年m月d日"',
        ];
    }

    public function messages()
    {
        return [
            'project_name.required' => 'プロジェクト名を入力してください。',
            'project_name.max' => 'プロジェクト名は:max文字以内で入力してください。',
            'project_comment.max' => 'コメントは:max文字以内で入力してください。',
            'client_id.required' => '顧客を選択してください。',
            'account_id.required' => '担当者を選択してください。',
            'use_day.required' => '利用日を選択してください。',
            'use_day.date_format' => '利用日が不正です。',
            'start_hour.required' => '利用開始時間を選択してください。',
            'start_hour.integer' => '利用開始時間が不正です。',
            'start_hour.between' => '利用開始時間を正しく選択してください。',
            'end_hour.required' => '利用終了時間を選択してください。',
            'end_hour.integer' => '利用終了時間が不正です。',
            'end_hour.between' => '利用終了時間を正しく選択してください。',
            'end_hour.gt' => '開始時間と終了時間を正しく設定してください。',
            'project_amount.required' => '利用料金を設定してください。',
            'project_amount.integer' => '利用料金は整数で入力してください。',
            'project_amount.min' => '利用料金は:min円以上で入力してください。',
            'invoice_deadline.required' => '料金受付締切日を選択してください。',
            'invoice_deadline.date_format' => '料金受付締切日が不正です。',
        ];
    }
}
