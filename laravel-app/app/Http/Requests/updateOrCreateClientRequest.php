<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class updateOrCreateClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_name' => 'required|max:20',
            'client_tel' => 'nullable|tel',
            'client_email' => 'nullable|email|max:191',
            'client_comment' => '',
            'is_black' => 'boolean',
        ];
    }

    public function messages()
    {
        return [
            'client_name.required' => '顧客名を入力してください。',
            'client_name.max' => '顧客名は:max文字以下で入力してください。',
            'client_tel.tel' => '不正な電話番号です。',
            'client_email.email' => '不正なメールアドレスです。',
            'client_email.max' => 'メールアドレスは191文字以下で入力してください。',
            'is_black.boolean' => '不正なリクエストです。',
        ];
    }
}
