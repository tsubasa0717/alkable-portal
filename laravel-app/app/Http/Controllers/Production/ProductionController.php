<?php

namespace App\Http\Controllers\Production;

use App\Libs\Common;
use App\Models\t_invoices;
use App\Models\t_projects;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductionController extends Controller
{
    // 利用日当日画面
    public function index(Request $request)
    {
        $page = [
            'title' => '利用日当日',
            'dir' => ['use']
        ];

        if(!isset($request->uuid))
        {
            return false;
        }

        $project = t_projects::whereProjectUuid($request->uuid)->first();

        // 仮予約の場合
        if($project->reservation_status == 0)
        {
            $invoice = t_invoices::whereProjectId($project->project_id)
                ->whereInvoiceType(0)
                ->first();

            return redirect('/invoice?uuid='.$invoice->invoice_uuid);
        }

        // 本予約の場合
        elseif($project->reservation_status == 1)
        {
            $project->termination_time = Carbon::parse($project->termination_time)->format('H:i');
            $termsOfService = json_decode(file_get_contents(storage_path('app/json/terms_of_service.json')),true);

            $data = [
                'page' => $page,
                'project' => $project,
                'termsOfService' => $termsOfService,
            ];

            return view('use/step1')->with($data);
        }

        // 利用中の場合
        elseif($project->reservation_status == 2)
        {
            $is_holiday = Common::isHoliday(Carbon::parse($project->start_time)->format('Y-m-d'))['status'] ? 1 : 0;

            $data = [
                'page' => $page,
                'project' => $project,
                'is_holiday' => $is_holiday
            ];

            return view('use/step2')->with($data);
        }
        // 利用待機中の場合
        elseif($project->reservation_status == 3)
        {
            $termsOfEndService = json_decode(file_get_contents(storage_path('app/json/terms_of_end_service.json')),true);
            $data = [
                'page' => $page,
                'project' => $project,
                'termsOfEndService' => $termsOfEndService,
            ];

            return view('use/step3')->with($data);
        }

        // その他
        else
        {
            return redirect('/project/show?uuid='.$project->project_uuid);
        }
    }
}
