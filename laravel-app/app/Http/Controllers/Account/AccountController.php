<?php

namespace App\Http\Controllers\Account;

use App\Models\t_accounts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    // 社員一覧
    public function index(Request $request)
    {
        $page = [
            'title' => '社員一覧',
            'dir' => ['account']
        ];

        $accounts = t_accounts::all();

        $data = [
            'page' => $page,
            'accounts' => $accounts,
        ];

        return view('account/index')->with($data);
    }

    // 社員登録
    public function create(Request $request)
    {
        $page = [
            'title' => '社員登録',
            'dir' => ['account', 'create'],
        ];

        $data = [
            'page' => $page,
        ];

        return view('account/create')->with($data);
    }

    // 社員編集
    public function edit(Request $request)
    {
        $page = [
            'title' => '社員編集',
            'dir' => ['account', 'edit'],
        ];

        $data = [
            'page' => $page,
        ];

        return view('account/edit')->with($data);
    }
}
