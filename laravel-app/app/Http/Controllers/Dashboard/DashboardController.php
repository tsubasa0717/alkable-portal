<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\t_projects;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    // ダッシュボード
    public function index(Request $request)
    {
        $page = [
            'title' => 'ダッシュボード',
            'dir' => ['']
        ];

        // 本日の予定取得
        $todayProjects = t_projects::whereDate('start_time',Carbon::today())->get();
        foreach ($todayProjects as $project)
        {
            $project->start_time = Carbon::parse($project->start_time)->format('H:i');
            $project->termination_time = Carbon::parse($project->termination_time)->format('H:i');
        }

        $data = [
            'page' => $page,
            'todayProjects' => $todayProjects,
        ];

        return view('index')->with($data);
    }
}
