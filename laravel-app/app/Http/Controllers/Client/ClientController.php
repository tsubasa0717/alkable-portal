<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Board\BoardController;
use App\Libs\Common;
use App\Models\t_clients;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ClientController extends Controller
{
    // 顧客一覧
    public function index(Request $request)
    {
        $page = [
            'title' => '顧客一覧',
            'dir' => ['client']
        ];

        $clients = t_clients::all();
        foreach ($clients as $client)
        {
            $client->client_tel = $client->client_tel != '' ? $client->client_tel : '登録なし';
            $client->client_email = $client->client_email != '' ? $client->client_email : '登録なし';
        }

        $data = [
            'page' => $page,
            'clients' => $clients,
        ];

        return view('client/index')->with($data);
    }

    // 顧客登録
    public function create(Request $request)
    {
        $page = [
            'title' => '顧客登録',
            'dir' => ['client','create']
        ];

        $form = [
            'client_name' => '',
            'client_tel' => '',
            'client_email' => '',
            'client_comment' => '',
            'client_uuid' => '',
            'is_black' => 0,
        ];

        $data = [
            'page' => $page,
            'form' => $form,
        ];

        return view('client/create')->with($data);
    }

    // 顧客登録
    public function edit(Request $request)
    {
        $page = [
            'title' => '顧客編集',
            'dir' => ['client','edit']
        ];

        if(!isset($request->uuid))
        {
            return false;
        }

        $client = t_clients::whereClientUuid($request->uuid)->first();

        $form = [
            'client_name' => $client->client_name,
            'client_tel' => $client->client_tel,
            'client_email' => $client->client_email,
            'client_comment' => $client->client_comment,
            'is_black' => $client->is_black,
            'client_uuid' => $client->client_uuid,
        ];

        $data = [
            'page' => $page,
            'form' => $form,
        ];

        return view('client/edit')->with($data);
    }

}
