<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Board\BoardController;
use App\Http\Controllers\ExternalAPI\GoogleCalendarController;
use App\Http\Requests\updateOrCreateClientRequest;
use App\Http\Requests\updateOrCreateInvoiceRequest;
use App\Http\Requests\updateOrCreateProjectRequest;
use App\Libs\Common;
use App\Models\t_clients;
use App\Models\t_invoices;
use App\Models\t_projects;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\CollectsResources;
use Illuminate\Support\Facades\Session;

class ReservationManagementController extends Controller
{
    // 顧客登録処理
    public function storeClient(updateOrCreateClientRequest $request)
    {
        if(!isset($request->client_name))
        {
            return false;
        }

        // Board登録
        $boardClient = BoardController::createClient([
            'name' => $request->client_name,
            'name_disp' => $request->client_name,
            'tel' => $request->client_tel,
            'to' => $request->client_email,
        ]);

        if(!isset($boardClient['id']))
        {
            return false;
        }

        // DB登録
        $dbClient = t_clients::create([
            'board_client_code' => $boardClient['id'],
            'client_uuid' => Common::uuid(),
            'client_name' => $boardClient['name'],
            'client_name_disp' => $boardClient['name_disp'],
            'client_email' => $boardClient['to'],
            'client_tel' => $boardClient['tel'],
            'client_comment' => htmlspecialchars($request->client_comment, ENT_QUOTES),
            'is_black' => 0,
        ]);

        $redirectPath = '/client/create';
        Session::flash('client-name', $boardClient['name']);
        Session::flash('flash_message_success', true);
        return redirect($redirectPath);

    }

    // 簡易版顧客登録処理
    public function storeQuickClient(Request $request)
    {
        $view_data = [];
        $response = [
            'status' => 0,
            'message' => '',
            'view_data' => $view_data,
        ];

        if(!isset($request->client_name))
        {
            return false;
        }

        // Board登録
        $boardClient = BoardController::createClient([
            'name' => $request->client_name,
            'name_disp' => $request->client_name,
        ]);

        if(!isset($boardClient['id']))
        {
            return false;
        }

        // DB登録
        $dbClient = t_clients::create([
            'board_client_code' => $boardClient['id'],
            'client_uuid' => Common::uuid(),
            'client_name' => $boardClient['name'],
            'client_name_disp' => $boardClient['name_disp'],
            'is_black' => 0,
        ]);

        $view_data = [
            'client' => $dbClient,
        ];

        $response['status'] = 1;
        $response['message'] = '';
        $response['view_data'] = $view_data;

        return response()->json($response);

    }

    // プロジェクト登録処理
    public function storeProject(updateOrCreateProjectRequest $request)
    {
        // DBプロジェクト登録
        $dbProject = t_projects::create([
            'project_uuid' => Common::uuid(),
            'project_name' => $request->project_name,
            'project_comment' => $request->project_comment,
            'google_calendar_code' => '',
            'client_id' => $request->client_id,
            'account_id' => $request->account_id,
            'reservation_status' => 0,
            'start_time' => Common::formatDate($request->use_day,$request->start_hour),
            'termination_time' => Common::formatDate($request->use_day,$request->end_hour),
        ]);

        // Boardプロジェクト登録
        $boardProject = BoardController::createProject([
            "name" => $request->project_name,
            "client_id"=> Common::changeClientId2Code($request->client_id),
            "user_id" => Common::changeAccountId2Code($request->account_id),
            "estimate_date" => Carbon::today()->toDateString(),
            "order_status" => 1,
            "invoice_timing_kbn"=> 1,
            "invoice_date"=> Carbon::today()->toDateString(),
            "tags" => ["あるかぶる案件","$dbProject->project_id"]
        ]);

        // Google Calendar登録
        $googleCalendarEvent = GoogleCalendarController::insert('gallery',[
            'summary' => $request->project_name,
            'location' => '東京都目黒区上目黒２丁目４１−９',
            'description' => $request->project_comment,
            'start' => $dbProject->start_time,
            'end' => $dbProject->termination_time,
        ]);
        $dbProject->update([
            'google_calendar_code' => $googleCalendarEvent['id'],
        ]);

        // Board見積り&請求登録
        $document_data = [
            "total" => $request->project_amount,
            "tax" => $request->project_amount * env('ConsumptionTaxFee')/100,
            "message" => "ギャラリーあるかぶる使用料",
            "details" => [
                [
                    "description" => "ギャラリーあるかぶる使用料",
                    "quantity" => 1,
                    "unit" => "式",
                    "unit_price" => $request->project_amount
                ],
            ]
        ];
        $boardInvoice = BoardController::updateInvoice($boardProject['invoices'][0]['id'],$document_data);
        $boardEstimate = BoardController::updateEstimate($boardProject['estimate']['id'],$document_data);

        // DB請求登録
        $dbInvoice = t_invoices::create([
            'board_project_code' => $boardProject['id'],
            'project_id' => $dbProject->project_id,
            'board_invoice_code' => $boardProject['invoices'][0]['id'],
            'board_estimate_code' => $boardProject['estimate']['id'],
            'board_receipt_code' => $boardProject['receipts'][0]['id'],
            'invoice_name' => "利用料金",
            'invoice_type' => 0,
            'invoice_amount' => $request->project_amount,
            'invoice_remain_amount' => $request->project_amount,
            'invoice_status' => 0,
            'invoice_deadline' => Common::formatDate($request->invoice_deadline,23,59,59),
        ]);

        $redirectPath = '/project/create';
        Session::flash('project-name', $request->project_name);
        Session::flash('flash_message_success', true);
        return redirect($redirectPath);

    }

    // 請求登録処理
    public function storeInvoice(updateOrCreateInvoiceRequest $request)
    {
        $project = t_projects::whereProjectId($request->project_id)->first();

        // Boardプロジェクト登録
        $boardProject = BoardController::createProject([
            "name" => Common::changeInvoiceType2Name($request->invoice_type),
            "client_id"=> Common::changeClientId2Code($project->client_id),
            "user_id" => Common::changeAccountId2Code($project->account_id),
            "estimate_date" => Carbon::today()->toDateString(),
            "order_status" => 1,
            "invoice_timing_kbn"=> 1,
            "invoice_date"=> Carbon::today()->toDateString(),
            "tags" => ["あるかぶる案件","$project->project_id"]
        ]);

        // Board見積り&請求登録
        $document_data = [
            "total" => $request->invoice_amount,
            "tax" => $request->invoice_amount * env('ConsumptionTaxFee')/100,
            "message" => Common::changeInvoiceType2Name($request->invoice_type),
            "details" => [
                [
                    "description" => Common::changeInvoiceType2Name($request->invoice_type),
                    "quantity" => 1,
                    "unit" => "式",
                    "unit_price" => $request->invoice_type
                ],
            ]
        ];
        $boardInvoice = BoardController::updateInvoice($boardProject['invoices'][0]['id'],$document_data);
        $boardEstimate = BoardController::updateEstimate($boardProject['estimate']['id'],$document_data);

        // DB請求登録
        $dbInvoice = t_invoices::create([
            'board_project_code' => $boardProject['id'],
            'project_id' => $project->project_id,
            'board_invoice_code' => $boardProject['invoices'][0]['id'],
            'board_estimate_code' => $boardProject['estimate']['id'],
            'board_receipt_code' => $boardProject['receipts'][0]['id'],
            'invoice_name' => Common::changeInvoiceType2Name($request->invoice_type),
            'invoice_type' => $request->invoice_type,
            'invoice_amount' => $request->invoice_amount,
            'invoice_remain_amount' => $request->invoice_amount,
            'invoice_status' => 0,
            'invoice_deadline' => Common::formatDate($request->invoice_deadline,23,59,59),
        ]);

        $redirectPath = '/invoice/create';
        Session::flash('invoice-name', Common::changeInvoiceType2Name($request->invoice_type));
        Session::flash('flash_message_success', true);
        return redirect($redirectPath);

    }
}
