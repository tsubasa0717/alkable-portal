<?php

namespace App\Http\Controllers\Project;

use App\Models\t_accounts;
use App\Models\t_clients;
use App\Models\t_invoices;
use App\Models\t_projects;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    // プロジェクト一覧
    public function index(Request $request)
    {
        $page = [
            'title' => 'プロジェクト一覧',
            'dir' => ['project'],
        ];

        // TODO: データベースを叩く回数を極限まで減らす努力
        $projects = t_projects::all();
        foreach ($projects as $project)
        {
            $project->start_time = Carbon::parse($project->start_time)->format('Y年m月d日 H時');
            $project->termination_time = Carbon::parse($project->termination_time)->format('H時');
        }

        $data = [
            'page' => $page,
            'projects' => $projects,
        ];

        return view('project/index')->with($data);
    }

    // プロジェクト登録
    public function create(Request $request)
    {
        $page = [
            'title' => 'プロジェクト登録',
            'dir' => ['project','create'],
        ];

        $clients = t_clients::all();
        $accounts = t_accounts::all();

        $form = [
            'project_name' => '',
            'project_comment' => '',
            'client_id' => '',
            'account_id' => '',
            'use_day' => '',
            'start_hour' => '',
            'end_hour' => '',
            'project_amount' => '',
            'invoice_deadline' => '',
        ];


        $data = [
            'page' => $page,
            'clients' => $clients,
            'accounts' => $accounts,
            'form' => $form
        ];

        return view('project/create')->with($data);
    }

    // プロジェクト編集
    public function edit(Request $request)
    {
        $page = [
            'title' => 'プロジェクト編集',
            'dir' => ['project','edit'],
        ];

        if(!isset($request->uuid))
        {
            return redirect('/project');
        }

        $project = t_projects::whereProjectUuid($request->uuid)->first();
        $invoice = t_invoices::whereProjectId($project->project_id)
            ->whereInvoiceType(0)
            ->first();
        $clients = t_clients::all();
        $accounts = t_accounts::all();

        $form = [
            'project_name' => $project->project_name,
            'project_comment' => $project->project_comment,
            'client_id' => $project->client_id,
            'account_id' => $project->account_id,
            'use_day' => Carbon::parse($project->start_time)->format('Y年m月d日'),
            'start_hour' => Carbon::parse($project->start_time)->format('H'),
            'end_hour' => Carbon::parse($project->termination_time)->format('H'),
            'project_amount' => $invoice->invoice_amount,
            'invoice_deadline' => Carbon::parse($invoice->invoice_deadline)->format('Y年m月d日'),
        ];


        $data = [
            'page' => $page,
            'project' => $project,
            'clients' => $clients,
            'accounts' => $accounts,
            'form' => $form
        ];

        return view('project/edit')->with($data);
    }

    // プロジェクト詳細
    public function show(Request $request)
    {
        $page = [
            'title' => 'プロジェクト詳細',
            'dir' => ['project','show'],
        ];

        if(!isset($request->uuid))
        {
            return redirect('/project');
        }

        $project = t_projects::whereProjectUuid($request->uuid)->first();
        $invoices = t_invoices::whereProjectId($project->project_id)
            ->orderBy('created_at','desc')->get();

        // view用整形
        $project->use_day = Carbon::parse($project->start_time)->format('Y/m/d');
        $project->start_time = Carbon::parse($project->start_time)->format('H:i');
        $project->termination_time = Carbon::parse($project->termination_time)->format('H:i');
        $project->project_comment = $project->project_comment != '' ? $project->project_comment : 'コメントなし';
        foreach ($invoices as $invoice)
        {
            $invoice->invoice_comment = $invoice->invoice_comment != '' ? $invoice->invoice_comment : 'なし';
            $invoice->invoice_day = Carbon::parse($invoice->created_at)->format('Y/m/d');
        }

        $data = [
            'page' => $page,
            'project' => $project,
            'invoices' => $invoices,
        ];

        return view('project/show')->with($data);
    }
}
