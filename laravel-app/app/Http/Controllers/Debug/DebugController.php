<?php

namespace App\Http\Controllers\Debug;

use App\Http\Controllers\Board\BoardController;
use App\Http\Controllers\ExternalAPI\GoogleCalendarController;
use App\Libs\Common;
use App\Models\t_accounts;
use App\Models\t_clients;
use App\Models\t_projects;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Mockery\Exception;
use Yasumi\Yasumi;

class DebugController extends Controller
{
    // デバック
    public function index(Request $request)
    {
        dd($accounts = BoardController::showUsers());
//        foreach ($accounts as $account)
//        {
//            // DBデータ
//            $account_data = [
//                'board_account_code' => $account['id'],
//                'last_name' => $account['last_name'],
//                'first_name' => $account['first_name'],
//                'email' => $account['email'],
//            ];
//
//            // ポータル内DB操作
//            t_accounts::updateOrCreate([
//                'board_account_code'=> $account['id']
//            ],$account_data);
//        }
//        dd($accounts);

//        dump(Carbon::parse('1 month')->endOfMonth());
//        dump(t_projects::whereDate('start_time','2019-01-08')->get());
//        $eId = '58p9m6hajbb38s4o311ouljacg';
//        $result = GoogleCalendarController::getEvent('gallery',$eId);
//        dump($result['id']);

//        $from = '2019-01-10 14:00:00';
//        $to = '2019-01-10 16:00:00';
//        $data = [
//            'summary' => 'テスト利用',
//            'location' => '東京都目黒区上目黒２丁目４１−９',
//            'description' => '',
//            'start' => $from,
//            'end' => $to,
//        ];
//        $options = [
//            'from' => $from,
//            'to' => $to,
//        ];
//        dump(GoogleCalendarController::insert('gallery',$data));
//        dump(GoogleCalendarController::update('gallery',$eId,$data));
//        dump(GoogleCalendarController::getList('gallery',$options));

//
//        dd(GoogleCalendarController::checkEmpty($from,$to));

//        $holidays = Yasumi::create('Japan', 2018, 'ja_JP');
//        $date = '2018-01-01';
//        $name = '平日';
//
//        foreach ($holidays as $holiday)
//        {
//            if($holiday->format('Y-m-d') == $date)
//            {
//                $name = $holiday->getName();
//            }
//        }

//        $day = '2018年12月29日';
//        dd(Common::isHoliday($day));

//        dump($holidays->getHolidayNames(new \DateTime('2018-12-23')));


    }

    // API系
    public function api(Request $request)
    {
//        $dbClient = t_clients::create([
//            'board_client_code' => 123421,
//            'client_uuid' => Common::uuid(),
//            'client_name' => 'aa',
//            'client_name_disp' => 'aa',
//            'client_email' => 'aa',
//            'client_tel' => 'aa',
//            'client_comment' => 'aa',
//            'is_black' => 0,
//        ]);
//        $client_data = [
//          'name' => '削除用顧客',
//          'name_disp' => 'RTC'
//        ];
//
//        $result = BoardController::createClient($client_data);
//        dd($result);


        // 顧客リスト取得
//        $result = BoardController::showClients();

        // 顧客取得
//        $client_code = 51695785;
//        $result = BoardController::showClient($client_code);

        // 顧客更新
//        $client_code = 51695785;
//        $client_data = [
//          'name' => 'リーテックコーポレーション改',
//          'name_disp' => 'newRTC'
//        ];
////
//        $result = BoardController::updateClient($client_code,$client_data);

        // 顧客削除
//        $client_code = 51695790;
//        $result = BoardController::deleteClient($client_code);

        // プロジェクト登録
//        $client_code = 51695785;
//        $staff_code = 38548600;
//        $project_data = [
//            "name" => "削除用案件",
//            "client_id"=> $client_code,
//            "user_id" => $staff_code,
//            "estimate_date" => "2019-08-11",
//            "order_status" => 1,
//            "invoice_timing_kbn"=> 1,
//            "invoice_date"=> "2019-10-11",
//            "tags" => ["あるかぶる案件"]
//        ];
//
//        $result = BoardController::createProject($project_data);

        // プロジェクトリスト取得
//        $result = BoardController::showProjects();

        // プロジェクト取得
//        $project_code = 83639451;
//        $result = BoardController::showProject($project_code);

        // プロジェクト更新
//        $project_code = 83639451;
//        $project_data = [
//            "name" => "サンプル案件改",
//        ];
//        $result = BoardController::updateProject($project_code,$project_data);

        // プロジェクト削除
//        $project_code = 83639457;
//        $result = BoardController::deleteProject($project_code);

        // 請求書リスト取得
//        $result = BoardController::showInvoices();


        // 請求書取得
//        $invoice_code = 30008297;
//        $result = BoardController::showInvoice($invoice_code);

        // 請求書更新
//        $invoice_code = 30008297;
//        $invoice_data = [
//            "total" => 300000,
//            "tax" => 24000,
//            "message" => "サンプル備考です。",
//            "details" => [
//                [
//                    "description" => "サンプル明細1",
//                    "quantity" => 1,
//                    "unit" => "式",
//                    "unit_price" => 100000
//                ],
//                [
//                    "description" => "サンプル明細2",
//                    "quantity" => 1,
//                    "unit" => "式",
//                    "unit_price" => 200000
//                ],
//            ]
//        ];
//        $result = BoardController::updateInvoice($invoice_code,$invoice_data);

        // 見積書取得
//        $estimate_code = 92982416;
//        $result = BoardController::showEstimate($estimate_code);

        // 見積書更新
//        $estimate_code = 92982416;
//        $estimate_data = [
//            "total" => 300000,
//            "tax" => 24000,
//            "message" => "サンプル備考です。",
//            "details" => [
//                [
//                    "description" => "サンプル明細1",
//                    "quantity" => 1,
//                    "unit" => "式",
//                    "unit_price" => 100000
//                ],
//                [
//                    "description" => "サンプル明細2",
//                    "quantity" => 1,
//                    "unit" => "式",
//                    "unit_price" => 200000
//                ],
//            ]
//        ];
//        $result = BoardController::updateEstimate($estimate_code,$estimate_data);

        // 領収書取得
//        $receipt_code = 2728711;
//        $result = BoardController::showReceipt($receipt_code);

        // 領収書更新
//        $receipt_code = 2728711;
//        $receipt_data = [
//            "total" => 300000,
//            "tax" => 24000,
//            "message" => "サンプル備考です。",
//            "details" => [
//                [
//                    "description" => "サンプル明細1",
//                    "quantity" => 1,
//                    "unit" => "式",
//                    "unit_price" => 100000
//                ],
//                [
//                    "description" => "サンプル明細2",
//                    "quantity" => 1,
//                    "unit" => "式",
//                    "unit_price" => 200000
//                ],
//            ]
//        ];
//        $result = BoardController::updateReceipt($receipt_code,$receipt_data);

        // 社員取得
//        $result = BoardController::optimizationAccounts();
//        $result = BoardController::optimizationAccounts();
//        var_dump($result);

//        return view('tr')->with($data);

    }


}