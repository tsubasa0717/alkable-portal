<?php

namespace App\Http\Controllers\Invoice;

use App\Models\t_invoices;
use App\Models\t_projects;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InvoiceController extends Controller
{
    // 請求一覧
    public function index(Request $request)
    {
        $page = [
            'title' => '請求一覧',
            'dir' => ['invoice'],
        ];

        $invoices = t_invoices::all();

        foreach ($invoices as $invoice)
        {
            $invoice->invoice_deadline = Carbon::parse($invoice->invoice_deadline)->format('Y/m/d');
        }

        $invoice_uuid = '';
        if(isset($request->uuid))
        {
            $invoice_uuid = $request->uuid;
        }

        $data = [
            'page' => $page,
            'invoices' => $invoices,
            'invoice_uuid' => $invoice_uuid,
        ];

        return view('invoice/index')->with($data);
    }

    // 請求登録
    public function create(Request $request)
    {
        $page = [
            'title' => '請求登録',
            'dir' => ['invoice','create'],
        ];

        $projects = t_projects::all();

        $project_id = '';
        if(isset($request->uuid))
        {
            $project_id = t_projects::whereProjectUuid($request->uuid)->first()->project_id;
        }

        $form = [
            'project_id' => $project_id,
            'invoice_type' => '',
            'invoice_amount' => '',
            'invoice_deadline' => '',
        ];

        $data = [
            'page' => $page,
            'projects' => $projects,
            'form' => $form,
        ];

        return view('invoice/create')->with($data);
    }

    // 請求登録
    public function edit(Request $request)
    {
        $page = [
            'title' => '請求編集',
            'dir' => ['invoice','edit'],
        ];

        if(!isset($request->uuid))
        {
            return redirect('/invoice');
        }

        $projects = t_projects::all();
        $invoice = t_invoices::whereInvoiceUuid($request->uuid)->first();

        $form = [
            'project_id' => $invoice->project_id,
            'invoice_type' => $invoice->invoice_type,
            'invoice_amount' => $invoice->invoice_amount,
            'invoice_deadline' => Carbon::parse($invoice->invoice_deadline)->format('Y年m月d日'),
        ];

        $data = [
            'page' => $page,
            'projects' => $projects,
            'invoice' => $invoice,
            'form' => $form,
        ];

        return view('invoice/edit')->with($data);
    }
}
