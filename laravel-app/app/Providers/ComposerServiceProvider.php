<?php

namespace App\Providers;

use App\Models\t_accounts;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;


class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function($view)
        {
            $site = config('site');
            $myAccount = Auth::user();

            $data = [
                'site' => $site,
                'myAccount' => $myAccount,
            ];

            $view->with($data);
        });
    }



    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
