<?php

namespace App\Models;

use App\Libs\Common;
use Illuminate\Database\Eloquent\Model;

class t_payments extends Model
{
    //
    protected $table = 't_payments';
    protected $primaryKey = 'payment_id';

    protected $fillable = [
        'payment_id','invoice_id', 'payment_amount'
    ];

    public $timestamps = true;

    // 新規作成の際に初期データ生成
    public static function boot()
    {
        parent::boot();
        self::creating(function(t_payments $t_payments){
            $t_payments->payment_uuid = Common::uuid();
        });
    }
}
