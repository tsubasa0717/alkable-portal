<?php

namespace App\Models;

use App\Libs\Common;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class t_invoices extends Model
{
    //
    protected $table = 't_invoices';
    protected $primaryKey = 'invoice_id';

    protected $fillable = [
        'project_id','board_project_code', 'board_invoice_code', 'board_estimate_code','board_receipt_code','invoice_name','invoice_type','invoice_amount','invoice_remain_amount','invoice_status','invoice_deadline'
    ];

    public $timestamps = true;

    // 新規作成の際に初期データ生成
    public static function boot()
    {
        parent::boot();
        self::creating(function(t_invoices $t_invoice){
            $t_invoice->invoice_uuid = Common::uuid();
        });
    }

    public function getInvoiceStatusNameAttribute()
    {
        $invoice_status_name = '';
        if($this->invoice_status == 0)
        {
            $invoice_status_name = '請求中';
        }
        elseif($this->invoice_status == 1)
        {
            $invoice_status_name = '請求中(一部支払い)';
        }
        elseif($this->invoice_status == 2)
        {
            $invoice_status_name = '支払い済み';
        }
        elseif($this->invoice_status == 100)
        {
            $invoice_status_name = '請求中(期日超過)';
        }

        return $invoice_status_name;
    }

    public function getInvoiceRemainDeadlineAttribute()
    {
        $remainDeadline = '';
        $today = Carbon::now();
        $deadline = Carbon::parse($this->invoice_deadline);

        if($today->gt($deadline) && $today->diffInDays($deadline)!=0)
        {
            $remainDeadline = '<b style="color:red">(超過'.$today->diffInDays($deadline).'日)</b>';
        }
        else if($today->diffInDays($deadline)<7 && $today->diffInDays($deadline)>0)
        {
            $remainDeadline = '<b>(残り'.$today->diffInDays($deadline).'日)</b>';
        }
        else if($today->diffInDays($deadline)==0)
        {
            $remainDeadline = '<b>(本日支払締切日)</b>';
        }

        return $remainDeadline;
    }

}
