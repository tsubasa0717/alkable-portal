<?php

namespace App\Models;

use App\Libs\Common;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class t_accounts extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [
//        'name', 'email', 'password',
//    ];
    protected $fillable = [
        'board_account_code', 'last_name', 'first_name','email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //
    protected $table = 't_accounts';
    protected $primaryKey = 'account_id';

    public $timestamps = true;

    // 新規作成の際に初期データ生成
    public static function boot()
    {
        parent::boot();
        self::creating(function(t_accounts $t_account){
            $t_account->account_uuid = Common::uuid();
            $t_account->password = password_hash('password', PASSWORD_BCRYPT);
        });
    }

}