<?php

namespace App\Models;

use App\Libs\Common;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class t_clients extends Model
{
    //
    protected $table = 't_clients';
    protected $primaryKey = 'client_id';

    protected $fillable = [
        'board_client_code', 'client_name', 'client_name_disp','client_email','client_tel','client_comment','is_black',
    ];

    public $timestamps = true;

    // 新規作成の際に初期データ生成
    public static function boot()
    {
        parent::boot();
        self::creating(function(t_clients $t_client){
            $t_client->client_uuid = Common::uuid();
            $t_client->is_black = 0;
        });
    }

    public function getUseTimesAttribute()
    {
        $useTimes = t_projects::whereClientId($this->client_id)->count();
        return $useTimes;
    }

    public function getSummaryTimeAttribute()
    {
        $summaryTime = 0;
        $projects = t_projects::whereClientId($this->client_id)->get();

        foreach ($projects as $project)
        {
            $start_time = new Carbon($project->start_time);
            $end_time = new Carbon($project->termination_time);
            $useHour = $end_time->diffInHours($start_time);
            $summaryTime += $useHour;
        }

        return $summaryTime;
    }

    public function getClientProjectsAttribute()
    {
        $projects = t_projects::whereClientId($this->client_id)->get();
        return $projects;
    }

    public function getLastUseDayAttribute()
    {
        $last_use_day = 'まだ利用がありません。';
        $project = t_projects::whereClientId($this->client_id)->orderBy('start_time','asc');
        if($project->exists())
        {
            $last_use_day = Carbon::parse($project->first()->start_time)->format('Y年m月d日');
        }

        return $last_use_day;
    }
}
