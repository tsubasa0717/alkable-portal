<?php

namespace App\Models;

use App\Libs\Common;
use Illuminate\Database\Eloquent\Model;

class t_projects extends Model
{
    //
    protected $table = 't_projects';
    protected $primaryKey = 'project_id';

    protected $fillable = [
        'project_id','project_name','project_comment','google_calendar_code','client_id','account_id','reservation_status','start_time','termination_time'
    ];

    public $timestamps = true;

    // 新規作成の際に初期データ生成
    public static function boot()
    {
        parent::boot();
        self::creating(function(t_projects $t_project){
            $t_project->project_uuid = Common::uuid();
        });
    }

    public function getClientNameAttribute()
    {
        $client = t_clients::whereClientId($this->client_id)->first();
        return $client->client_name;
    }

    public function getAccountNameAttribute()
    {
        $account = t_accounts::whereAccountId($this->account_id)->first();
        return $account->last_name.' '.$account->first_name;
    }

    public function getReservationStatusNameAttribute()
    {
        $reservation_status_name = '';
        if($this->reservation_status == 0)
        {
            $reservation_status_name = '仮予約';
        }
        elseif($this->reservation_status == 1)
        {
            $reservation_status_name = '本予約';
        }
        elseif($this->reservation_status == 2)
        {
            $reservation_status_name = '利用中';
        }
        elseif($this->reservation_status == 3)
        {
            $reservation_status_name = '利用終了待機';
        }
        elseif($this->reservation_status == 4)
        {
            $reservation_status_name = '利用終了';
        }
        elseif($this->reservation_status == 5)
        {
            $reservation_status_name = '利用終了(違約金支払い済み)';
        }
        elseif($this->reservation_status == 100)
        {
            $reservation_status_name = '違約金発生中';
        }

        return $reservation_status_name;
    }
}
