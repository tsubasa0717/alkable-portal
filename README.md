# 導入方法
### コンテナ起動
```
docker-compose up -d
```

### パーミッション変更
```
docker exec -it laravel chmod 777 storage  
```
```
docker exec -it laravel chmod 777 ./bootstrap/cache/
```

### Laravel ライブラリインストール
composerがインストールされていなければインストール 
``` 
cd laravel-app  
```
```
composer update
```

### Laravel .env .gitignore 設置
.envファイルと.gitignoreファイルを /laravel-app 直下に配置

### Laravel App-key取得
```
docker exec -it laravel php artisan key:generate
```

### DB migrate,seed
```
docker exec -it laravel php artisan migrate  
```
```
docker exec -it laravel php artisan db:seed
```

### アクセス
http://localhost:80